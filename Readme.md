Solar Survey
==================

 Perform a solar site survey using your smartphone!
 
 10/19/2020 - Note that this app is not functioning at present due to a variety of changes in the Android platform and backend APIs that were used to download solar data. Unfortunately I don't have time to support it at the present, but contributors to the codebase toward updating the app are welcome!

Privacy Policy
----------------------
User location data and camera images may be used by this app for the purposes of computing and displaying information to the user. Data may be saved on the phone's internal storage, and may be accessed (loaded) during subsequent uses of the app. No user data is transmitted by the app.

Alpha/Beta Test Information
----------------------
1. Visit the [Google Play Beta Opt-In Link](https://play.google.com/apps/testing/edu.psu.hn.solarsurvey) to opt into the beta:

1. This should provide you with a link to install the app to your phone via the Google Play website, which will also allow you to receive updated versions automatically to your phone.

To provide Alpha/Beta feedback:
1. Join the Google Group named [Solar Survey](https://groups.google.com/forum/#!forum/solar-survey):



Version History
------------------

- v 0.5.2a: TBD
	* Placeholder


- v 0.5.1a: Aug-03-2015
	* Backend speed improvements to saving
	* Backend speed improvements to the AR display
	* Fixed an AR speed bug introduced by Android 5.1 on Moto X (2nd Gen)
	* Fixed a bug where "rename" operations failed to save the diffuse shade factor value


- v 0.5.0a: Sept-05-2014
    * THIS UPDATE WILL CAUSE INCOMPATIBILITY WITH PREVIOUS PROJECT SAVES
    * Diffuse shading factor calculation now works
    * Updated file outputs to contain diffuse shade factor and other information
    * Validation updates to code


- v 0.4.5a: Sept-05-2014
    * Big News: Perez model accurate to SAM within ~0.1%
    * Overhauled solar calculations. Now uses the same methodology for sun position as SAM
    * Shading now uses a decimal shading factor, rather than binary shaded/unshaded value 
    * Backend changes: representation of time in weather data objects is now local
    * Bugfix: Sometimes calculations based on loaded data didn't match saved exactly
    

- v 0.4.4a: Sept-03-2014
    * Removed ACRA crash reporting in favor of basic Google reporting
    * Cleaned up a few stray files in the calculations library
    * Fixed a couple display issues on the graphs (more to come on that front)
    * Bugfix: Crash when loading graphs
    * Bugfix: Problem where Sun Chart Plot would reset shading calculations

- v 0.4.3a: Jul-04-2014 (ASES Conference Release)
	* Switched codebase to Android Studio, loss of code backwards compatibility with this step
	* Improved the look of the camera-based screen buttons
	* Performance improvements on the camera overlays
	* Added isotropic sky and Perez models, but these are untested and should be considered as such.

- v 0.4.2a: Jul-01-2014
    * Switched crash reporting to ACRA
    * Improved overall look and feel
    * Added a SAM hourly shading output file
    * Added a average day chart for each month
    * Added shaded values to charts

- v 0.4.1a: Jun-24-2014
    * Changed navigation to be more intuitive.
    * Added tutorials. Click the question mark icon for help about the current page.
    * Bugfixes based on user feedback.

- v 0.4a: Jun-19-2014 
    * Initial Alpha Release

Acknolwedgements
------------------

####Contributors
- Jesse Fox
- Niraj Patel
- Kurt Schaarschmidt
- Matthew Caccese
- Dan Yankowsky
- Art by Emily Harding


Third-Party Code and Libraries Used
--------------------------
- [AndroidPlot](http://androidplot.com/) - Under [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0)
- [ShowcaseView](http://amlcurran.github.io/ShowcaseView/) - Under [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0)
- [Apache Commons Math](http://commons.apache.org/proper/commons-math/) - Under [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0)
- [Apache Commons IO](http://commons.apache.org/proper/commons-io/) - Under [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0)

Copyright
---------------------------

(C) 2014 - Joe Ranalli

Licensed under Mozilla Pulic License 2.0

see LICENSE.txt for details