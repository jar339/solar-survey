package edu.psu.hn.solarsurvey.io.parsing;

public interface Parser {
    void processline(String line);
    void complete();
}
