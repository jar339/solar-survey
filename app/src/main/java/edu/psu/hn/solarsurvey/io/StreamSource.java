package edu.psu.hn.solarsurvey.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;

public interface StreamSource {
    Reader getStream() throws IOException; //TODO change to InputStreamReader
}
