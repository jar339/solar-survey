package edu.psu.hn.solarsurvey.model.Events;

public interface EventListener<T> {
    public void onEvent(T t);
}
