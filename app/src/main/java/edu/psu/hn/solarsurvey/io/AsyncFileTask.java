package edu.psu.hn.solarsurvey.io;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import edu.psu.hn.solarsurvey.io.parsing.Parser;

public class AsyncFileTask extends AsyncTask<Void, Integer, Void> {

    private String LOG_TAG = "AsyncFileTask";

    List<DownloadFileTaskListener> listeners = new ArrayList<DownloadFileTaskListener>();
    Parser parser;
    StreamSource streamSource;

    public AsyncFileTask(Parser parser, StreamSource streamSource, DownloadFileTaskListener... listeners){
        super();
        for (DownloadFileTaskListener listener:listeners) {
            this.listeners.add(listener);
        }
        this.streamSource = streamSource;
        this.parser = parser;
    }

    protected Void doInBackground(Void... voids) {

        Log.d(LOG_TAG,"Begin Download");
        try {
            Reader stream = streamSource.getStream();

            //long completeFileSize = httpConnection.getContentLength();
            long completeFileSize = 8762;
            Log.d(LOG_TAG,"File Size: "+completeFileSize+" bytes.");

            BufferedReader in = new BufferedReader(stream);

            long linesread = 0;
            String s = null;
            Log.d(LOG_TAG,"Passing lines to parser.");
            while ((s = in.readLine()) != null) {
                linesread += 1;

                // calculate progress
                final int currentProgress = (int) ((((double)linesread) / ((double)completeFileSize)) * 100d);
                publishProgress(currentProgress);

                parser.processline(s);
                //Log.v(LOG_TAG,s); //logging overkill
            }

            Log.d(LOG_TAG,"Closing stream");
            in.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPreExecute(){
        for (DownloadFileTaskListener listener:listeners) {
            listener.onPreExecute();
        }
    }
    @Override
    protected void onProgressUpdate(Integer... progress) {
        for (DownloadFileTaskListener listener:listeners) {
            listener.onProgressUpdate(progress[0]);
        }
    }

    @Override
    protected void onPostExecute(Void pass) {
        this.parser.complete();
        for (DownloadFileTaskListener listener:listeners) {
            listener.onPostExecute(parser);
        }
    }


    public interface DownloadFileTaskListener{
        public void onPreExecute();
        public void onPostExecute(Parser parser);
        public void onProgressUpdate(int progress);
    }
}


