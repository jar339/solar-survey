/*
 * This file is part of Solar Survey.
 *  Copyright Joe Ranalli 2014
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package edu.psu.hn.solarsurvey.io.filesave;

import android.content.Context;
import android.util.Log;

import edu.psu.hn.solarsurvey.BuildConfig;
import edu.psu.hn.solarsurvey.io.AsyncFileTask;
import edu.psu.hn.solarsurvey.io.parsing.Parser;
import edu.psu.hn.solarsurvey.io.parsing.SavedFileParser;
import edu.psu.hn.solarsurvey.model.WeatherData;

/**
 * An implementation of CSV file to store weather data to eliminate the need to relaod from the web.
 *
 */
public class ProjectWeatherFile extends CSVFile {

    String LOG_TAG = "ProjectWeatherFile";

	public ProjectWeatherFile(String directory, String filename) {
		super(directory, filename);

		//override default filename schema
		this.mFileName = filename;
	}

	public ProjectWeatherFile(String ProjectName) {
		this(ProjectStorageDirectories.PROJECT_DIRECTORY+ProjectName,ProjectStorageDirectories.WEATHER_FILENAME);
	}


	/**
	 * Write the relevant subset of the meterological data needed to recreate it in memory.
	 */
	public void saveFile(Context c, WeatherData data) {

		if (BuildConfig.DEBUG){
			Log.d(this.LOG_TAG,"Weather saveFile() Entered");
		}

		//empty file for rewrite
		this.flush();

		//write header
		this.writeLine("GHI,DNI,DHI,d,m,y,time");

		for(int i = 0; i < data.GHI.length; i++)
		{
			//Combine a row's worth of data
			String tmp_csv = String.format("%f,%f,%f,%d,%d,%d,%f",
                data.GHI[i],
                data.DNI[i],
                data.DHI[i],
                data.d[i],
                data.m[i],
                data.y[i],
                data.localtime[i]);

			this.writeLine(tmp_csv);
		}

		//Broadcast
		this.broadcast(c);

		Log.d(this.LOG_TAG,"saveFile() Complete");

	}


	AsyncFileTask.DownloadFileTaskListener listener = new AsyncFileTask.DownloadFileTaskListener() {
		@Override
		public void onPreExecute() {

		}

		@Override
		public void onPostExecute(Parser parser) {

		}

		@Override
		public void onProgressUpdate(int progress) {

		}
	};

	/**
	 * Load meterological data from a previously stored file.
	 */
	public void loadFile() {
		SavedFileParser p = new SavedFileParser(mFileName);
		AsyncFileTask task = new AsyncFileTask(p,p,listener);
		task.execute();
	}
}
