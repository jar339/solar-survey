package edu.psu.hn.solarsurvey.model;

import android.location.Location;

import edu.psu.hn.solarsurvey.io.StreamSource;
import edu.psu.hn.solarsurvey.io.parsing.WeatherParser;
import edu.psu.hn.solarsurvey.io.parsing.TMYParser;
import edu.psu.hn.solarsurvey.model.Events.EventBroadcaster;
import edu.psu.hn.solarsurvey.model.Events.EventListener;

public class ProjectSite extends EventBroadcaster<ProjectSiteData> {
    private Location siteLoc;
    private TMYParser weatherParser;

    private boolean siteLocValid = false;

    public void setLocation(double lat, double lon){
        Location temploc = new Location("Manual");
        temploc.setLatitude(lat);
        temploc.setLongitude(lon);
        setLocation(temploc);
    }
    public void setLocation(Location location){
        siteLocValid = false;
        siteLoc = location;
        onLocationChange();
        weatherParser = new TMYParser(getLatitude(),getLongitude());
    }


    public ProjectSiteData getSiteData(){ return getBroadcastData();  }
    public double getLatitude(){ return siteLoc.getLatitude(); }
    public double getLongitude(){
        return siteLoc.getLongitude();
    }

    private void onLocationChange(){
        siteLocValid = true;
        broadcast(getBroadcastData());
    }

    private ProjectSiteData getBroadcastData(){
        ProjectSiteData broadcastData = new ProjectSiteData(siteLoc);
        return broadcastData;
    }

    public WeatherParser getParser(){
        if (siteLocValid) {
            TMYParser parser = weatherParser;
            return parser;
        } else {
            throw new IllegalStateException("Site not initialized");
        }
    }

    public StreamSource getURLSource(){
        if (siteLocValid) {
            return new TMYParser(getLatitude(),getLongitude());
        } else {
            throw new IllegalStateException("Site not initialized");
        }
    }

    public void addWeatherListener(EventListener<WeatherData> listener){
            weatherParser.addListener(listener); //should throw an error if it's null.
    }
    public void removeWeatherListener(EventListener<WeatherData> listener){
        if (weatherParser != null) {
            weatherParser.removeListener(listener);
        }
    }
}
