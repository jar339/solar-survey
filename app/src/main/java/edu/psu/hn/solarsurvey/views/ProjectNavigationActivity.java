package edu.psu.hn.solarsurvey.views;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.NavUtils;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;

import edu.psu.hn.solarsurvey.R;

public class ProjectNavigationActivity extends AppCompatActivity {

    private String LOG_TAG = "NavigationActivity";

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_site_setup:
                    switchFragment(new ProjectSiteFragment());
                    return true;
                case R.id.navigation_array_setup:
                    switchFragment(new ProjectArrayFragment());
                    return true;
                case R.id.navigation_shading_measurement:
                    switchFragment(new ProjectShadingFragment());
                    return true;
                case R.id.navigation_graphs:
                    switchFragment(new ProjectGraphFragment());
                    return true;
            }
            return false;
        }
    };

    /**
     * Activate the site configuration fragment.
     */
    public void switchFragment(Fragment fragment) {
        FragmentManager manager = getSupportFragmentManager();
        manager.beginTransaction().replace(R.id.fragment_container, fragment).commit();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_navigation);
        String projectName = getIntent().getStringExtra(ProjectChooserActivity.PROJECTNAME);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(true); // Hide the action bar title
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(projectName);



        //Setup the fragments and start with the site
        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        switchFragment(new ProjectSiteFragment());

        //Initialize the location permissions
        requestLocationPermission();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    /**
     * Perform a permission request for ACCESS_COARSE_LOCATION if not already active.
     * @return
     */
    public boolean requestLocationPermission() {
        Log.d(LOG_TAG,"Check location permissions");
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_COARSE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(this)
                        .setTitle(R.string.request_location_permission)
                        .setMessage(R.string.error_location_permission)
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(ProjectNavigationActivity.this,
                                        new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION);
                            }
                        })
                        .create()
                        .show();


            } else {
                Log.d(LOG_TAG,"Request location permission.");
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.d(LOG_TAG,"Permission for locations granted.");
                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                } else {
                    Log.d(LOG_TAG,"Permission for locations denied.");
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
            }
        }
    }


}
