package edu.psu.hn.solarsurvey.model;

import android.location.Location;

import edu.psu.hn.solarsurvey.io.parsing.WeatherParser;

public class WeatherData {
    public final Location dataLoc;
    public final double[] GHI;
    public final double[] DNI;
    public final double[] DHI;
    public final int[] y;
    public final int[] d;
    public final int[] m;
    public final double[] localtime;

    public WeatherData(WeatherParser parser){
        dataLoc = parser.getLocation();
        GHI = parser.getGHI();
        DNI = parser.getDNI();
        DHI = parser.getDHI();
        y = parser.getYear();
        d = parser.getDay();
        m = parser.getMonth();
        localtime = parser.getLocalTime();
    }

    public double getAnnualGHI(){
        return sum(GHI);
    }

    public Location getLocation(){
        return dataLoc;
    }
    public double getLatitude(){
        return dataLoc.getLatitude();
    }    public double getLongitude(){
        return dataLoc.getLongitude();
    }


    private double sum(double[] array){
        double sum=0;
        for (int i = 0; i < array.length; i++) {
            sum+=array[i];
        }
        return sum;
    }
}
