package edu.psu.hn.solarsurvey.io.filesave;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.widget.EditText;
import android.widget.LinearLayout;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.List;

import edu.psu.hn.solarsurvey.model.Events.EventBroadcaster;

public class ProjectNamer extends EventBroadcaster<List<ProjectFileData>> {

    String LOG_TAG = "ProjectNamer";

    private List<ProjectFileData> mProjectList;

    public ProjectNamer(){
        searchStorageDirectory();
    }

    public List<ProjectFileData> getProjects(){
        if (mProjectList ==null) {
            searchStorageDirectory();
        }
        return mProjectList;

    }

    /**
     * Show a list of projects that could be loaded
     */
    private void searchStorageDirectory(){
        //based on http://stackoverflow.com/questions/3592717/choose-file-dialog

        mProjectList = new ArrayList<ProjectFileData>();

        final String[] fileList;
        String fullPath = ProjectStorageDirectories.getProjectDirectory();
        File path = new File(fullPath);

        try {
            path.mkdirs();
        }
        catch(SecurityException e) {
            Log.e("Oops", "unable to write on the sd card " + e.toString());
        }

        if(path.exists()) {
            FilenameFilter filter = new FilenameFilter() {
                public boolean accept(File dir, String filename) {
                    File sel = new File(dir, filename);
                    return sel.isDirectory();
                }
            };
            fileList = path.list(filter);
        }
        else {
            fileList = new String[0];
        }


        for (String name:fileList){
            ProjectFileData project = new ProjectFileData();
            project.setName(name);
            mProjectList.add(project);
        }

        this.broadcast(mProjectList);
    }


    /**
     * Show a prompt asking the user to name the project, and pass execution if the name exists.
     */
    public void promptForName(final Context context){

        AlertDialog.Builder editalert = new AlertDialog.Builder(context);

        editalert.setTitle("Name Your Project");
        editalert.setMessage("Enter new project name...");
        editalert.setCancelable(false);

        final EditText input = new EditText(context);
        input.setHint("Enter Name Here");
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);
        editalert.setView(input);

        editalert.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        String name = input.getText().toString();
                        createProject(name, context);
                    }
                });

        editalert.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        editalert.show();
    }

    public void deleteProject(ProjectFileData project, Context context){
        deleteProject(project.name, context);
    }
    public void deleteProject(final String name, Context context){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        // set title
        alertDialogBuilder.setTitle("Confirm Delete");

        // set dialog message
        alertDialogBuilder
                .setMessage("Delete project \""+name+"\"?" +
                        "\n\nThis will delete all project files and cannot be undone!")
                .setCancelable(false)
                .setPositiveButton("Confirm",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        deleteProjectDirectory(name);

                    }
                })
                .setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                });

        // create alert dialog
        alertDialogBuilder.show();

    }

    /**
     * Delete a project directory completely
     */
    private void deleteProjectDirectory(String name){
        File f = new File(ProjectStorageDirectories.getProjectDirectory()+"/"+name);
        FileUtils.deleteQuietly(f);
        searchStorageDirectory();
    }

    private void createProject(String name, Context context){
        File dirfile = new File(ProjectStorageDirectories.getProjectDirectory()+name);
        dirfile.mkdirs();

        CSVFile file = new CSVFile(ProjectStorageDirectories.getProjectDirectory()+name,"testfile.csv");
        file.writeKeyValPair("TestKey","TestVal");
        file.broadcast(context);
        file.close();
        searchStorageDirectory();
    }
}
