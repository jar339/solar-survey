package edu.psu.hn.solarsurvey.views;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import edu.psu.hn.solarsurvey.ControllerSingleton;
import edu.psu.hn.solarsurvey.R;
import edu.psu.hn.solarsurvey.io.AsyncFileTask;
import edu.psu.hn.solarsurvey.io.parsing.Parser;
import edu.psu.hn.solarsurvey.model.Events.EventListener;
import edu.psu.hn.solarsurvey.model.ProjectSiteData;
import edu.psu.hn.solarsurvey.model.WeatherData;


public class ProjectSiteFragment extends Fragment implements View.OnClickListener {

    private String LOG_TAG = "ProjectSiteFragment";

    TextView mDLProgressLabel;
    ProgressBar mDLProgress;
    Button mDLButton;
    ImageButton mGetLocationButton;
    ProgressBar mLocProgress;

    EditText mLatitude;
    EditText mLongitude;

    View mDataLayout;
    TextView mDataGHIValue;
    TextView mDataSiteCoords;
    TextView mDataSideDist;

    ControllerSingleton controller;

    LocationManager mLocationManager;
    Location mLastLocation;
    final int locationTimeout_ms = 3000;


    public ProjectSiteFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_project_site, container, false);
        return inflater.inflate(R.layout.fragment_project_site, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        mDLProgressLabel = view.findViewById(R.id.dlProgressLabel);
        mDLProgress = view.findViewById(R.id.dlprogress);
        mDLButton = view.findViewById(R.id.dlbutton);
        mDLButton.setOnClickListener(this);
        mLatitude = view.findViewById(R.id.latitude);
        mLongitude = view.findViewById(R.id.longitude);
        mGetLocationButton = view.findViewById(R.id.findLocationButton);
        mGetLocationButton.setOnClickListener(this);
        mLocProgress = view.findViewById(R.id.locationProgressBar);
        setDLMode(false);

        mDataLayout = view.findViewById(R.id.LocationDataLayout);
        mDataGHIValue = view.findViewById(R.id.GHIValue);
        mDataSiteCoords = view.findViewById(R.id.siteloc);
        mDataSideDist = view.findViewById(R.id.siteDist);

        controller = ControllerSingleton.getInstance();
        controller.addSiteListener(siteListener);
    }


    @Override
    public void onDestroyView(){
        super.onDestroyView();
        mDLProgress = null;
        mDLProgressLabel = null;
        mLatitude = null;
        mLongitude = null;
        mDLButton = null;
        mDataSiteCoords = null;
        mDataLayout = null;
        mDataGHIValue = null;
        mDataSideDist = null;
        mGetLocationButton = null;
        mLocProgress = null;

        controller.cancelDownload();
        controller.removeSiteListener(siteListener);
        controller.removeWeatherListener(weatherListener);

        Log.d(LOG_TAG,"Check location permissions.");
        if (ContextCompat.checkSelfPermission(this.getActivity(),
                Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {

            Log.d(LOG_TAG,"Fragment closing, stop location updates.");
            if (mLocationManager !=null) {
                mLocationManager.removeUpdates(locationListener);
            }
        }
    }
    @Override
    public void onDestroy(){
        super.onDestroy();
        controller = null;
    }

    AsyncFileTask.DownloadFileTaskListener downloadListener = new AsyncFileTask.DownloadFileTaskListener() {
        public void onPreExecute() {
            setDLMode(true);
            mDLProgress.setIndeterminate(true);
        }
        public void onPostExecute(Parser parser) {
            setDLMode(false);
        }
        public void onProgressUpdate(int progress){
            mDLProgress.setIndeterminate(false);
            mDLProgressLabel.setText(String.valueOf(progress)+"%");
            mDLProgress.setProgress(progress);
        }
    };

    EventListener<WeatherData> weatherListener = new EventListener<WeatherData>(){
        @Override
        public void onEvent(WeatherData weatherData) {
            mDataLayout.setVisibility(View.VISIBLE);
            mDataGHIValue.setText(Html.fromHtml(String.valueOf(weatherData.getAnnualGHI())+" Wh/m<sup>2</sup>"));

            double lat = weatherData.getLatitude();
            double lon = weatherData.getLongitude();
            mDataSiteCoords.setText("Lat: "+String.valueOf(lat)+"\u00b0; Lon: "+String.valueOf(lon)+"\u00b0;");

            mDataSideDist.setText(String.valueOf(weatherData.getLocation().distanceTo(controller.getSiteData().location)/1000)+" km");
        }
    };

    EventListener<ProjectSiteData> siteListener = new EventListener<ProjectSiteData>() {
        @Override
        public void onEvent(ProjectSiteData projectSiteData) {
            setCoordinateValues(projectSiteData.location);
        }
    };

    @Override
    public void onClick(View v) {
        if (v.getId()== R.id.dlbutton){
            Log.d(LOG_TAG,"Download Click.");
            onDownloadClick();
        } else if (v.getId() == R.id.findLocationButton){
            Log.d(LOG_TAG,"Get Location Click.");
            onGetLocationClick();
        }
    }

    private void onGetLocationClick(){
        Log.d(LOG_TAG,"Trying to get live location.");
        Log.d(LOG_TAG,"Check location permissions");
        // Acquire a reference to the system Location Manager
        if (ContextCompat.checkSelfPermission(this.getActivity(),
                Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {

            Log.d(LOG_TAG,"Create the location manager.");
            mLocationManager = (LocationManager) this.getActivity().getSystemService(Context.LOCATION_SERVICE);


            // Register the downloadListener with the Location Manager to receive location updates
            try {
                Log.d(LOG_TAG,"Register for updates");
                setGetLocationMode(true);
                mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
                mLastLocation = mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                new Handler().postDelayed(locTimeout, locationTimeout_ms);
            } catch (SecurityException e) {
                Toast.makeText(getActivity(), R.string.error_location_permission, Toast.LENGTH_LONG).show();
            }
        }

    }

    // Define a downloadListener that responds to location updates
    LocationListener locationListener = new LocationListener() {
        public void onLocationChanged(Location location) {
            Log.d(LOG_TAG,"Location found setting values.");
            // Called when a new location is found by the network location provider.
            mLastLocation = location;
            controller.setLocation(location);
            //setCoordinateValues(mLastLocation);
            Log.d(LOG_TAG,"Stop location updates.");
            mLocationManager.removeUpdates(locationListener);
            setGetLocationMode(false);
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {}

        public void onProviderEnabled(String provider) {}

        public void onProviderDisabled(String provider) {}
    };

    private void setCoordinateValues(Location location){
        mLatitude.setText(String.valueOf(location.getLatitude()));
        mLongitude.setText(String.valueOf(location.getLongitude()));
    }

    private void onDownloadClick(){

        double lat;
        double lon;
        try {
            lat = Double.parseDouble(mLatitude.getText().toString());
        } catch (NumberFormatException e) {
            mLatitude.setError("Value must be a valid number.");
            return;
        }
        try {
            lon = Double.parseDouble(mLongitude.getText().toString());
        } catch (NumberFormatException e) {
            mLongitude.setError("Value must be a valid number.");
            return;
        }


        controller.setLocation(lat,lon);
        controller.downloadData(downloadListener,weatherListener);
    }

    private void setGetLocationMode(boolean searching){
        if (searching){
            mLocProgress.setVisibility(View.VISIBLE);
            mGetLocationButton.setVisibility(View.INVISIBLE);
        } else {
            mLocProgress.setVisibility(View.INVISIBLE);
            mGetLocationButton.setVisibility(View.VISIBLE);
        }
    }

    private void setDLMode(boolean downloading){
        if (downloading){
            mDLButton.setVisibility(View.INVISIBLE);
            mDLProgress.setVisibility(View.VISIBLE);
            mDLProgressLabel.setVisibility(View.VISIBLE);
        } else {
            mDLProgressLabel.setText("");
            mDLButton.setVisibility(View.VISIBLE);
            mDLProgress.setVisibility(View.INVISIBLE);
            mDLProgressLabel.setVisibility(View.INVISIBLE);
        }
    }

    private Runnable locTimeout = new Runnable(){
        public void run() {
            Log.d(LOG_TAG,"Timing out location");
            Toast.makeText(getActivity(), R.string.error_location_timeout, Toast.LENGTH_LONG).show();
            locationListener.onLocationChanged(mLastLocation);
        }
    };
}
