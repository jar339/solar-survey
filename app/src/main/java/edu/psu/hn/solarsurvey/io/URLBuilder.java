package edu.psu.hn.solarsurvey.io;

import java.util.Arrays;
import java.util.List;

public class URLBuilder {

    //See: https://developer.nrel.gov/docs/solar/nsrdb/
    final static String APIKEY = APIKeys.APIKEY;
    final static String YOUREMAIL = APIKeys.YOUREMAIL;
    final static List<String> ALLOWABLE = Arrays.asList(
            "1998","1999","2000",
            "2001","2002","2003","2004","2005",
            "2006","2007","2008","2009","2010",
            "2011","2012","2013","2014","2015",
            "2016","2017",
            "tmy","tmy-2016","tmy-2017","tdy-2017","tgy-2017");
            //for 1yr choices are: 1998, 1999, 2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017
            //for TMY choices are: tmy, tmy-2016, tmy-2017, tdy-2017, tgy-2017

    // Set leap year to true or false. True will return leap day data if present, false will not.
    final static String leap_year = "false";
    // Set time interval in minutes, i.e., "30" is half hour intervals. Valid intervals are 30 & 60.
    final static String interval = "60";
    // True for UTC, False for Local
    final static String utc = "false"; //SAM requires UTC=false

    //Possible APIs
    //String apicall = "https://developer.nrel.gov/api/solar/nsrdb_psm3_download.csv";
    //String apicall = "https://developer.nrel.gov/api/nsrdb_api/solar/nsrdb_psm3_tmy_download.csv";

    public static String getPSM3URL(double lat, double lon, String year){

        //See: https://developer.nrel.gov/docs/solar/nsrdb/
        String apicall = "https://developer.nrel.gov/api/solar/nsrdb_psm3_download.csv";

        //Choose Attributes
        String attributes = "ghi,dhi,dni";
        //for 1yr choices are: air_temperature, clearsky_dhi, clearsky_dni, clearsky_ghi, cloud_type, dew_point, dhi, dni, fill_flag, ghi, relative_humidity, solar_zenith_angle, surface_albedo, surface_pressure, total_precipitable_water, wind_direction, wind_speed
        //for TMY choices are: dhi, dni, ghi, dew_point, air_temperature, surface_pressure, wind_direction, wind_speed, surface_albedo

        // Choose year of data
        assert(ALLOWABLE.contains(year));
        //String year = "2010";

        String url = apicall +
                "?wkt=POINT(" + lon + "%20" + lat + ")" +
                "&names=" + year +
                "&leap_day=" + leap_year +
                "&interval=" + interval +
                "&utc=" + utc +
                "&email=" + YOUREMAIL +
                "&api_key=" + APIKEY +
                "&attributes=" + attributes;

        return url;
    }
}
