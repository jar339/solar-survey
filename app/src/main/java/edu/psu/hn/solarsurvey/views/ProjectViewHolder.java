package edu.psu.hn.solarsurvey.views;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import edu.psu.hn.solarsurvey.R;
import edu.psu.hn.solarsurvey.io.filesave.ProjectFileData;

//https://androidexamples4u.wordpress.com/material-design/recyclerview-and-cardview/creating-custom-recycler-adapter/
public class ProjectViewHolder extends RecyclerView.ViewHolder {

    TextView title;
    ImageView delete;
    ImageView projectIcon;

    public ProjectViewHolder(@NonNull View itemView) {
        super(itemView);
        title = itemView.findViewById(R.id.projectName);
        delete = itemView.findViewById(R.id.img_row_delete);
        projectIcon = itemView.findViewById(R.id.projectIcon);
        title.setOnClickListener(OnChooseProjectClick);
        projectIcon.setOnClickListener(OnChooseProjectClick);
    }

    public void setData(ProjectFileData current, int position){
        this.title.setText(current.getName());
    }

    View.OnClickListener OnChooseProjectClick = new View.OnClickListener(){

        @Override
        public void onClick(View v) {
            Context context = v.getContext();
            Intent openProject = new Intent(context,ProjectNavigationActivity.class);
            openProject.putExtra(ProjectChooserActivity.PROJECTNAME,title.getText());
            context.startActivity(openProject);
        }
    };

}
