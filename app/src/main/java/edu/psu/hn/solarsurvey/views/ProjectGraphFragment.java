package edu.psu.hn.solarsurvey.views;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import edu.psu.hn.solarsurvey.ControllerSingleton;
import edu.psu.hn.solarsurvey.R;


public class ProjectGraphFragment extends Fragment {

    private String LOG_TAG = "ProjectGraphFragment";

    private ControllerSingleton controller;

    public ProjectGraphFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_project_site, container, false);
        return inflater.inflate(R.layout.fragment_project_graph, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        controller = ControllerSingleton.getInstance();
    }


    @Override
    public void onDestroyView(){
        super.onDestroyView();

    }
    @Override
    public void onDestroy(){
        super.onDestroy();
        controller = null;
    }


}
