package edu.psu.hn.solarsurvey.views;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import edu.psu.hn.solarsurvey.ControllerSingleton;
import edu.psu.hn.solarsurvey.R;
import edu.psu.hn.solarsurvey.io.filesave.ProjectFileData;


public class ProjectRecycleAdapter extends RecyclerView.Adapter<ProjectViewHolder> {

    List<ProjectFileData> mData;
    LayoutInflater mInflater;

    public ProjectRecycleAdapter(Context context, List<ProjectFileData> data){
        mData = data;
        mInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public ProjectViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = mInflater.inflate(R.layout.list_project_card,viewGroup,false);
        ProjectViewHolder holder = new ProjectViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ProjectViewHolder projectViewHolder, final int i) {
        ProjectFileData current = mData.get(i);
        projectViewHolder.setData(current,i);

        projectViewHolder.delete.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ProjectFileData theRemovedItem = mData.get(i);

                ControllerSingleton.getInstance().deleteProject(theRemovedItem,v.getContext());
            }
        });

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }
}
