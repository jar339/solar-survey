/*
 * This file is part of Solar Survey.
 *  Copyright Joe Ranalli 2014
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package edu.psu.hn.solarsurvey.io.filesave;

import java.io.File;
import java.io.FileOutputStream;

import android.content.Context;
import android.content.Intent;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import edu.psu.hn.solarsurvey.BuildConfig;

/**
 * An abstraction for storage files in the External Storage directory.
 *
 */
public class SimpleFile {
	protected String LOG_TAG = "SimpleFile";
	protected boolean APPEND_FLAG = true;
	
	protected String mFileDir; 		//stores the writeLine directory
	protected String mFileName;		//stores the complete file name

	protected File mStorageDir;			//stores the directory obejct
	protected File mFileObject;					//stores the file obejct
	protected FileOutputStream mFileStream;		//stores the file output stream


	
	protected boolean mFileInitialized = false;
	
	/**
	 * Accept a directory and file name to create a file.
	 * @param directory
	 * @param filename
	 */
	public SimpleFile(String directory, String filename) {

			//get the storage directory
			File extFilesDir = Environment.getExternalStorageDirectory();
			
			//append the passed directory string to the storage director
			if (directory.contains(extFilesDir.toString())){ //Leave off storage dir if already present
				this.mFileDir = directory;
			} else {
				this.mFileDir = extFilesDir.toString() + "/" + directory;
			}
			//Append a slash if needed
			if (!this.mFileDir.substring(this.mFileDir.length()-1).equals("/")){
				this.mFileDir += "/";
			}

            //DateFormat df = new SimpleDateFormat("MMddyyyy_kkmmss"); //Can be used to clean up the timestamp df.format()

			this.mFileName = filename;
	}
	

	/**
	 * Broadcast the creation of the file to the Android file system
	 * @param c
	 */
	public void broadcast(Context c){
		//This notifies android that the file exists and should be accessible when connected to a PC.
		Log.d(LOG_TAG,"Broadcasting: "+mFileObject.toString() );
		c.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(mFileObject)));
	}

	/**
	 * Delete file and recreate and empty file
	 */
	public void flush() {	
		this.mFileObject = new File(this.mFileDir + this.mFileName);
		if(this.mFileObject.exists())
		{
			if(this.mFileObject.delete())
			{
				try {
					this.mFileObject.createNewFile();
				}
				catch(java.io.IOException e) {
					Log.e(this.LOG_TAG,"Error recreating file!");
				}
			}
		}
	}
	
	/**
	 * Delete a file from disk
	 */
	public void delete() {
		this.mFileObject = new File(this.mFileDir + this.mFileName);
		if(this.mFileObject.exists())
		{
			this.mFileObject.delete();
		}
	}	

	
	/**
	 * The file storage location
	 */
	public String toString()
	{
		return this.mFileDir + this.mFileName;
		
	}
	
	/**
	 * The save directory
	 * @return
	 */
	public String getDirectory(){
		return this.mFileDir;
	}
	
	/**
	 * The name of the file
	 * @return
	 */
	public String getFileName(){
		return this.mFileName;
	}
	
	/**
	 * Open a file and prepare for writing. If the file exists, 
	 * this obtains a File object pointing at the file. 
	 */
	public void createFile(){
		if (BuildConfig.DEBUG){
		  Log.d(this.LOG_TAG,"Creating File...");
		  Log.d(this.LOG_TAG,mFileDir+mFileName);
		}
		
		  //make a file object for the directory we need
		  this.mStorageDir = new File(this.mFileDir);
		  //create it if it doesn't exist
		  if (!mStorageDir.exists()) { mStorageDir.mkdirs(); }
		  
		  //create a file object for the data file
		  this.mFileObject = new File(this.mFileDir + this.mFileName);
		  //create the file if it doesn't exist
		  if (!this.mFileObject.exists()) {
			  try {
				this.mFileObject.createNewFile();
				mFileInitialized = true;
			  }
			  catch(java.io.IOException e) { 
				Log.e(this.LOG_TAG,"Error creating file!");
				mFileInitialized = false;
		      }
		  } else {
              mFileInitialized = true;
          }
	}
	
	public void write(byte[] data){
  		write(data,"N/A");
	}
	
	public void write(byte[] data, String stringData){
  		if (!mFileInitialized){
  			createFile();
  		}
		try{
            if (this.mFileStream == null){
                if (BuildConfig.DEBUG){
                    Log.d(this.LOG_TAG,"Opening Output Stream...");
                }
                this.mFileStream = new FileOutputStream(this.mFileObject, APPEND_FLAG);
            }

			if (BuildConfig.DEBUG){
				Log.d(this.LOG_TAG,"Writing Data:" + stringData);
			}
			this.mFileStream.write(data);

  		} catch (Exception e) {
  			e.printStackTrace();
  		}
	}

    public void close(){
        try{
            if (BuildConfig.DEBUG){
                Log.d(this.LOG_TAG,"Closing Output Stream...");
            }
            this.mFileStream.close();
            this.mFileStream = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
