package edu.psu.hn.solarsurvey.io.parsing;

import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;

public class SavedFileParser extends WeatherParser {

    final String file;

    int linesRead = 0;

    public SavedFileParser(String filename){
        file = filename;
    }

    @Override
    public void processline(String line) {
        switch (linesRead) {
            case 0: //the file headers
                break;
            default://actual data
                int row = linesRead - 1;
                String[] coldata = line.split(",");
                GHI[row] = Double.parseDouble(coldata[0]);
                DNI[row] = Double.parseDouble(coldata[1]);
                DHI[row] = Double.parseDouble(coldata[2]);
                day[row] = Integer.parseInt(coldata[3]);
                month[row] = Integer.parseInt(coldata[4]);
                year[row] = Integer.parseInt(coldata[5]);
                localtime[row] = Integer.parseInt(coldata[6]);
                break;

        }

        linesRead +=1;
    }

    @Override
    public void complete() {
        broadcast(this.getWeatherData());
    }

    @Override
    public Reader getStream() throws IOException {
        return new FileReader(file);
    }
}
