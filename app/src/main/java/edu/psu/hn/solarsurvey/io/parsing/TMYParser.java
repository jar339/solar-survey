package edu.psu.hn.solarsurvey.io.parsing;

import android.util.Log;

import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;

import edu.psu.hn.solarsurvey.io.URLBuilder;

public class TMYParser extends WeatherParser {

    String LOG_TAG = "TMYParser";

    int linesRead = 0;

    double targetlat;
    double targetlon;

    public TMYParser(double latitude, double longitude){
        targetlat = latitude;
        targetlon = longitude;
    }



    @Override
    public void processline(String line) {
        switch (linesRead) {
            case 0: //the file headers
                break;
            case 1: //the file parameters
                String[] cols = line.split(",");
                siteID = Integer.parseInt(cols[1]);
                lat = Double.parseDouble(cols[5]);
                lon = Double.parseDouble(cols[6]);
                timeZone = Integer.parseInt(cols[7]);
            case 2: //the column headers
                break;
            default://actual data
                int row = linesRead - 3;
                String[] coldata = line.split(",");
                year[row] = Integer.parseInt(coldata[0]);
                month[row] = Integer.parseInt(coldata[1]);
                day[row] = Integer.parseInt(coldata[2]);
                int hour = Integer.parseInt(coldata[3]);
                int minute = Integer.parseInt(coldata[4]);
                localtime[row] = hour+minute/60.0;
                GHI[row] = Double.parseDouble(coldata[5]);
                DHI[row] = Double.parseDouble(coldata[6]);
                DNI[row] = Double.parseDouble(coldata[7]);
                break;

        }

        linesRead +=1;
    }

    public void complete(){
        isValid = true;
        broadcast(this.getWeatherData());
    }

    @Override
    public Reader getStream() {
        String url = URLBuilder.getPSM3URL(targetlat,targetlon,"tmy");
        Log.d(LOG_TAG,"Open URL: "+url);
        try {
            URL fileURL = new URL(url);
            HttpURLConnection httpConnection = (HttpURLConnection) (fileURL.openConnection());
            return new InputStreamReader(httpConnection.getInputStream());
        } catch (Exception e){
            e.printStackTrace();
            return null;
        }

    }

}
