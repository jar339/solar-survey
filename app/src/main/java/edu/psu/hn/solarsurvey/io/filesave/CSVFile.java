/*
 * This file is part of Solar Survey.
 *  Copyright Joe Ranalli 2014
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package edu.psu.hn.solarsurvey.io.filesave;

//for file interaction


/**
 * An abstraction for CSV files that will save data to the storage space.
 *
 */
public class CSVFile extends SimpleFile {

	protected String LOG_TAG = "CSVFile";
	
	public CSVFile(String directory, String filename){
		super(directory,filename);
	}

	/**
	 * Write a string as a line of the CSV file.
	 * @param output
	 */
	public void writeLine(String output) {
		if (!output.contains("\n")){
			output += "\r\n";
		}
			
		try {
			write(output.getBytes(),output);
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}
	
	/**
	 * Write a key, value pair to the CSV file.  Format is "key,value\n"
	 * @param key
	 * @param val
	 */
	public void writeKeyValPair(String key, String val) {
		String tmp = key + "," + val;
		this.writeLine(tmp);
	}
}
