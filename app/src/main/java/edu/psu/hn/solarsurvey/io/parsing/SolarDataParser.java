package edu.psu.hn.solarsurvey.io.parsing;///*
// * This file is part of Solar Survey.
// *  Copyright Joe Ranalli 2014
// *
// *  This Source Code Form is subject to the terms of the Mozilla Public
// *  License, v. 2.0. If a copy of the MPL was not distributed with this
// *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
// */
//
//package edu.psu.hn.solarsurvey.io.parsing;
//
//import java.io.BufferedInputStream;
//import java.io.BufferedReader;
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.InputStreamReader;
//import java.net.URL;
//import java.text.DecimalFormat;
//import java.util.Calendar;
//import java.util.GregorianCalendar;
//import java.util.LinkedHashSet;
//import java.util.Set;
//
//import edu.psu.hn.solarsurvey.solarmath.DataObjects.StationInfo;
//import edu.psu.hn.solarsurvey.solarmath.DataObjects.StationInfoAll;
//import edu.psu.hn.solarsurvey.solarmath.DataObjects.WeatherFileData;
//import edu.psu.hn.solarsurvey.solarmath.SolarCalculator;
//import callbacks.IStringCallback;
//
//public final class SolarDataParser {
//
//	// ************************ GENERAL ***************************
//
//	/**
//	 * Count the lines in an Input Stream
//	 * @param is the InputStream
//	 * @return integer number of lines
//	 * @throws IOException
//	 */
//	private static int countLinesStream(InputStream is) throws IOException{
//		//http://stackoverflow.com/questions/453018/number-of-lines-in-a-file-in-java
//		BufferedInputStream bs = new BufferedInputStream(is);
//		try {
//		        byte[] c = new byte[1024];
//		        int count = 0;
//		        int readChars = 0;
//		        boolean empty = true;
//		        while ((readChars = bs.read(c)) != -1) {
//		            empty = false;
//		            for (int i = 0; i < readChars; ++i) {
//		                if (c[i] == '\n') {
//		                    ++count;
//		                }
//		            }
//		        }
//		        return (count == 0 && !empty) ? 1 : count;
//		    } finally {
//		        //bs.close();
//		    }
//	}
//
//	/**
//	 * Parse Station MetaData for All Stations, and put the data into object format
//	 * @param is
//	 * @param inds - An object initialized to the correct column indices for the relevant data.
//	 * @return
//	 */
//	public static StationInfoAll parseMetaDataStream(InputStream is, MetaInds inds){
//		BufferedReader br = null;
//		String line = "";
//		String separator = ",";
//
//		StationInfoAll output = new StationInfoAll();
//
//		try {
//
//			br = new BufferedReader(new InputStreamReader(is));
//
//			// First line contains the headers, skip
//			line = br.readLine();
//			String[] strings = line.split(separator);
//
//			while ((line = br.readLine()) != null) {
//
//				// separate columns from line
//				strings = line.split(separator);
//
//				// data values
//				StationInfo si = new StationInfo.Builder()
//				.ID(Integer.parseInt(strings[inds.ID]))		.state(strings[inds.state])
//				.city(strings[inds.city])					.latitude(Double.parseDouble(strings[inds.lat]))
//				.longitude(Double.parseDouble(strings[inds.lon]))	.elevation(Double.parseDouble(strings[inds.elev]))
//				.timezone(Integer.parseInt(strings[inds.timzn]))
//				.build();
//
//				output.add(si);
//			}
//			//return out;
//			return output;
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return null;
//	}
//
//	// ************************* TMY3 ****************************
//
//	/**
//	 * Apply the file name format to a TMY3 Station ID
//	 * @param stationID
//	 * @return
//	 */
//	public static String getTMY3FileName(int stationID){
//		String suffix = "TYA.csv";
//		return String.valueOf(stationID) + suffix;
//	}
//	/**
//	 * Combine a TMY3 Station ID into the web URL where the CSV file is stored
//	 * @param stationID the station ID
//	 * @return the web URL
//	 */
//	public static String getTMY3FileURL(int stationID){
//		// example http://rredc.nrel.gov/solar/old_data/nsrdb/1991-2005/data/tmy3/722285TY.csv
//		String baseURL = "http://rredc.nrel.gov/solar/old_data/nsrdb/1991-2005/data/tmy3/";
//		return baseURL + getTMY3FileName(stationID);
//	}
//
//	public static String getTMY3MetaDataFileName(){
//		return "TMY3_StationsMeta.csv";
//	}
//	public static String getTMY3MetaDataFileURL(){
//		return "http://rredc.nrel.gov/solar/old_data/nsrdb/1991-2005/tmy3/TMY3_StationsMeta.csv";
//	}
//
//	// Parsing function for TMY3 files
//	/**
//	 * Parse the TMY3 file as an InputStream
//	 * @param is
//	 * @param fileName
//	 * @param isc - An object that will handle a callback for String progress updates. (Update will be a percentage)
//	 * @return
//	 */
//	public static WeatherFileData parseTMY3Stream(InputStream is,String fileName,IStringCallback isc){
//		//This is the actual parsing code, everything else is a helper to generate the streams or
//		//provide alternate headers without a callback.
//
//		//Column indices in a TMY3 format file
//		int GHIind = 4;
//		int DHIind = 10;
//		int DNIind = 7;
//		int timeind = 1;
//		int dateind = 0;
//
//		// TODO this isn't working correctly due to the double reading of the file.  Investigate.
//		int len = 8760; // (Hardcode)
////		try {
////			len = countLinesStream(is);
////			len = len-2;
////		} catch (IOException e1) {
////			e1.printStackTrace();
////			len = 8760;
////		}
//
//		// Allocate the storage for the data
//		double[] GHI = new double[len];
//		double[] DNI = new double[len];
//		double[] DHI = new double[len];
//		int[] n = new int[len];
//		int[] d = new int[len];
//		int[] m = new int[len];
//		int[] y = new int[len];
//		double[] localtime = new double[len];
//		int[] hrOfYr = new int[len];
//
//		// Data that will be obtained from the header
//		double latitude;
//		double longitude;
//		int timeZone;
//		String locName;
//		int nHrs;
//		int nDays;
//
//		// Other needs
//		BufferedReader br = null;
//		String line = "";
//		String separator = ",";
//		GregorianCalendar gc = new GregorianCalendar();
//
//		// Begin Parsing
//		try {
//			br = new BufferedReader(new InputStreamReader(is));
//
//			// First line contains the location info
//			line = br.readLine();
//			String[] strings = line.split(separator);
//
//			locName = strings[1].replace("\"", "") + ", " + strings[2];
//			timeZone = (int)Double.parseDouble(strings[3]);
//			latitude = Double.parseDouble(strings[4]);
//			longitude = Double.parseDouble(strings[5]);
//
//			// Second line contains the column headers, skip
//			line = br.readLine();
//
//			// Subsequent lines are the data
//			int i = 0;
//			while ((line = br.readLine()) != null) {
//				if (i%100 == 0){
//					if (isc != null){
//						DecimalFormat fmt = new DecimalFormat("0");
//						isc.StringCallback(fmt.format(i/8760.0*100));
//					}
//				}
//			    // separate columns from line
//				strings = line.split(separator);
//
//				// data values
//				GHI[i] = Double.parseDouble(strings[GHIind]);
//				DHI[i] = Double.parseDouble(strings[DHIind]);
//				DNI[i] = Double.parseDouble(strings[DNIind]);
//
//				// additional parsing needed to split the date into month/day/year
//				String[] dateStrs = strings[dateind].split("/");
//				m[i] = Integer.parseInt(dateStrs[0]);
//				d[i] = Integer.parseInt(dateStrs[1]);
//				y[i] = Integer.parseInt(dateStrs[2]);
//
////				// Calculate day of year based on the date info given using a Calendar
////				gc.set(y[i], m[i]-1, d[i]);
////				n[i] = gc.get(Calendar.DAY_OF_YEAR);
//
//				// Calculate day of year based on the date info using the SAM method
//				int[] mDays = new int[] {31,28,31,30,31,30,31,31,30,31,30,31};
//				int[] cumDays = new int[] {0,31,59,90,120,151,181,212,243,273,304,334,365};
//				int k = 0;
//				if (y[i]%4==0 && m[i]>2)
//				{
//					k=1;
//				}
//				n[i] = cumDays[m[i]-1]+d[i]+k;
//
//				// additional parsing to split the time into hours
//				String[] timeStrs = strings[timeind].split(":");
//				double loctime = Double.parseDouble(timeStrs[0]);
//				localtime[i] = loctime-1;
//				hrOfYr[i]=i;
//				i++;
//			}
//
//			// Get unique hours and days.
//			Set<Integer> hourKeys = new LinkedHashSet<Integer>();
//			Set<Integer> dayKeys = new LinkedHashSet<Integer>();
//			for (double timei : localtime){
//				hourKeys.add( (int) timei );
//			}
//			for (int ni : n){
//				dayKeys.add(ni);
//			}
//            //TODO Investigate why key sizes weren't matching up.
//			//nHrs = hourKeys.size();
//			//nDays = dayKeys.size();
//            nHrs = 24;
//            nDays = 365;
//
//			// Use the Builder to create the RawSolarData object
//			WeatherFileData data = new WeatherFileData.Builder()
//												.GHI(GHI)				.DNI(DNI)				.DHI(DHI)
//												.n(n)					.d(d)					.m(m)  	.y(y)
//												.localtime(localtime)				.latitude(latitude)		.longitude(longitude)
//												.timeZone(timeZone)		.fileName(fileName)		.locName(locName)
//												.nHrs(nHrs)				.nDays(nDays) .hrOfYr(hrOfYr)
//												.build();
//			return data;
//
//		} catch (IOException e) {
//			e.printStackTrace();
//		} finally {
//			if (br != null) {
//				try {
//					br.close();
//				} catch (IOException e) {
//					e.printStackTrace();
//				}
//			}
//		}
//		return null; // Hopefully we don't get here.
//	}
//
//	// Alternate function calls for the parsing
//	/**
//	 * Parse the TMY3 file as an InputStream.
//	 * @param is
//	 * @param fileName
//	 * @return
//	 */
//	public static WeatherFileData parseTMY3Stream(InputStream is,String fileName){
//		return parseTMY3Stream(is,fileName,null);
//	}
//	/**
//	 * Parse the TMY3 file based on a URL
//	 * @param fileNameURL
//	 * @return a RawSolarData object containing the whole set of information
//	 */
//	public static WeatherFileData parseTMY3URL(String fileNameURL){
//		return parseTMY3URL(fileNameURL,null);
//	}
//	/**
//	 * Parse a TMY3 file based on a web address.
//	 * @param fileNameURL
//	 * @param isc
//	 * @return
//	 */
//	public static WeatherFileData parseTMY3URL(String fileNameURL,IStringCallback isc){
//		InputStream urlStream;
//		try {
//			urlStream = new BufferedInputStream(new URL(fileNameURL).openStream());
//			WeatherFileData rsd = parseTMY3Stream(urlStream,fileNameURL,isc);
//			return rsd;
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		return null;
//	}
//
//
//
//
//
//
//
//
//	// ******************************************** SUNY ***************************************
//
//	/**
//	 * Parse a SUNY Actual Meteorological Year (AMY) data file
//	 * @param filename
//	 * @return a RawSolarData object containing the whole set of information
//	 */
//	public static WeatherFileData parseSUNY(String filename){
//		// TODO Implement SUNY Parsing
//		return null;
//	}
//
//	/**
//	 * Combine a SUNY Station ID into the web URL where the CSV file is stored
//	 * @param stationID the station ID
//	 * @param year the year of AMY data desired
//	 * @return the web URL
//	 */
//	public static String getSUNYFileName(int stationID,int year){
//		// example http://rredc.nrel.gov/solar/old_data/nsrdb/1991-2010/data/hourly/722287/722287_1991_solar.csv
//		String baseURL = "http://rredc.nrel.gov/solar/old_data/nsrdb/1991-2010/data/hourly/";
//		String stationStr = String.valueOf(stationID);
//		String suffix = "_solar.csv";
//		return baseURL + stationStr + "/" + stationStr + "_" + String.valueOf(year) + suffix;
//	}
//
//	public static String getSUNYMetaDataFileName(){
//		//return "NSRDB_StationsMeta_alphabetical.csv";
//		return "NSRDB_StationsMeta.csv";
//	}
//	public static String getSUNYMetaDataFileURL(){
//		return "http://rredc.nrel.gov/solar/old_data/nsrdb/1991-2010/NSRDB_StationsMeta.csv";
//	}
//
//
//
//}
