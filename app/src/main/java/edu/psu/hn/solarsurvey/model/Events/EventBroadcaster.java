package edu.psu.hn.solarsurvey.model.Events;

import java.util.ArrayList;
import java.util.List;

public class EventBroadcaster<T> implements IEventBroadcaster<T> {
    private List<EventListener<T>> listeners = new ArrayList<EventListener<T>>();

    @Override
    public void addListener(EventListener<T> listener) {
        listeners.add(listener);
    }

    @Override
    public void removeListener(EventListener<T> listener) {
        if (listeners.contains(listener)){
            listeners.remove(listener);
        }
    }

    public void broadcast(T t){
        for (EventListener<T> listener:listeners){
            listener.onEvent(t);
        }
    }
}
