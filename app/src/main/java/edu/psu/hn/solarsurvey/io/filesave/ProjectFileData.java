package edu.psu.hn.solarsurvey.io.filesave;

public class ProjectFileData {
    public String name;

    public void setName(String name){
        this.name = name;
    }

    public String getName(){
        return name;
    }
}
