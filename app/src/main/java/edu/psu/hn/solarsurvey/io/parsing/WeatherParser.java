package edu.psu.hn.solarsurvey.io.parsing;

import android.location.Location;

import edu.psu.hn.solarsurvey.io.StreamSource;
import edu.psu.hn.solarsurvey.model.Events.EventBroadcaster;
import edu.psu.hn.solarsurvey.model.WeatherData;

public abstract class WeatherParser extends EventBroadcaster<WeatherData> implements Parser, StreamSource {
    int siteID;
    double lat;
    double lon;
    int timeZone;
    int[] year = new int[8760];
    int[] month= new int[8760];
    int[] day= new int[8760];

    double[] localtime = new double[8760];

    double[] GHI= new double[8760];
    double[] DHI= new double[8760];
    double[] DNI= new double[8760];
    boolean isValid = false;

    public double getLatitude(){
        return lat;
    }
    public double getLongitude(){
        return lon;
    }
    public Location getLocation(){
        Location loc = new Location("Manual");
        loc.setLatitude(lat);
        loc.setLongitude(lon);
        return loc;
    }

    public int[] getYear(){
        return year;
    }
    public int[] getMonth(){
        return month;
    }
    public int[] getDay(){
        return day;
    }
    public int[] getHour() {
        int[] hour = new int[localtime.length];
        for (int i = 0; i < localtime.length; i++) {
            hour[i] = (int)Math.floor(localtime[i]);
        }
        return hour;
    }
    public int[] getMinute() {
        int[] min = new int[localtime.length];
        for (int i = 0; i < localtime.length; i++) {
            min[i] = (int)(localtime[i]%1)*60;
        }
        return min;
    }
    public double[] getGHI() {
        return GHI;
    }
    public double[] getDHI() {
        return DHI;
    }
    public double[] getDNI() {
        return DNI;
    }
    public double[] getLocalTime(){ return localtime; }

    public WeatherData getWeatherData() throws IllegalStateException{
        if (isValid){
            return new WeatherData(this);
        } else {
            throw new IllegalStateException("Data parsing not completed.");
        }
    }

}
