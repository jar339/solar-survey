package edu.psu.hn.solarsurvey.model.Events;

public interface IEventBroadcaster<T> {
    public void addListener(EventListener<T> listener);
    public void removeListener(EventListener<T> listener);
}
