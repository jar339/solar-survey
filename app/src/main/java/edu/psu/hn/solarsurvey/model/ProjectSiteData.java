package edu.psu.hn.solarsurvey.model;

import android.location.Location;

public class ProjectSiteData {
    public final Location location;
    ProjectSiteData(Location location){
        this.location = location;
    }
}
