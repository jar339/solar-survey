/*
 * This file is part of Solar Survey.
 *  Copyright Joe Ranalli 2014
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package edu.psu.hn.solarsurvey;


import android.content.Context;
import android.location.Location;

import java.util.List;

import edu.psu.hn.solarsurvey.io.AsyncFileTask;
import edu.psu.hn.solarsurvey.io.filesave.ProjectFileData;
import edu.psu.hn.solarsurvey.io.filesave.ProjectNamer;
import edu.psu.hn.solarsurvey.io.parsing.Parser;
import edu.psu.hn.solarsurvey.model.Events.EventListener;
import edu.psu.hn.solarsurvey.model.ProjectSite;
import edu.psu.hn.solarsurvey.model.ProjectSiteData;
import edu.psu.hn.solarsurvey.model.WeatherData;

/**
 * A master wrapper for the controller involved in representing a project.
 *
 */
public class ControllerSingleton {
    // See example Singleton https://gist.github.com/Akayh/5566992

    ProjectSite site = new ProjectSite();
    ProjectNamer namer = new ProjectNamer();
    AsyncFileTask mDownloadTask;


	private static ControllerSingleton mInstance = null;
	private ControllerSingleton(){	}
	
	/**
	 * Acquire the instance of the singleton
	 * @return
	 */
	public static ControllerSingleton getInstance(){
		if (mInstance == null){
			mInstance = new ControllerSingleton();
		}
		return mInstance;
	}

	//***** Download Weather Files *****
    public void downloadData(AsyncFileTask.DownloadFileTaskListener downloadListener, EventListener<WeatherData> weatherListener) {
        site.addWeatherListener(weatherListener);
	    mDownloadTask = new AsyncFileTask(site.getParser(), site.getURLSource(),downloadListener, onCompletelistener);
        mDownloadTask.execute();
    }

    public void removeWeatherListener(EventListener<WeatherData> weatherListener){
	    site.removeWeatherListener(weatherListener);
    }

    /**
     * Respond to PostExecute by nulling out mDownloadTask so it doesn't get re-called accidentally
     */
    AsyncFileTask.DownloadFileTaskListener onCompletelistener = new AsyncFileTask.DownloadFileTaskListener(){
        @Override
        public void onPreExecute() {}
        @Override
        public void onPostExecute(Parser parser) {
            mDownloadTask = null;}
        @Override
        public void onProgressUpdate(int progress) {}
    };

    /**
     * Cancel the download task
     */
    public void cancelDownload(){
	    if (mDownloadTask != null) {
            mDownloadTask.cancel(true);
        }
        mDownloadTask = null;
    }

    //**** The Site Setup ****
    public void setLocation(double lat, double lon) {
        site.setLocation(lat,lon);
    }
    public void setLocation(Location location) {
        site.setLocation(location);
    }
    public ProjectSiteData getSiteData() {return site.getSiteData();}
    public void addSiteListener(EventListener<ProjectSiteData> listener){
        site.addListener(listener);
    }
    public void removeSiteListener(EventListener<ProjectSiteData> listener){
        site.removeListener(listener);
    }


    //**** The Project File Setup ****
    public void addProjectFileListener(EventListener<List<ProjectFileData>> listener){
        namer.addListener(listener);
    }
    public void removeProjectFileListener(EventListener<List<ProjectFileData>> listener){
        namer.removeListener(listener);
    }
    public void promptForFileName(Context context){
        namer.promptForName(context);
    }
    public List<ProjectFileData> getExistingProjects(){
        return namer.getProjects();
    }
    public void deleteProject(ProjectFileData project, Context context){
        namer.deleteProject(project, context);
    }
}
