/*
 * This file is part of Solar Survey.
 *  Copyright Joe Ranalli 2014
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package edu.psu.hn.solarsurvey.io.filesave;

import android.os.Environment;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by Ranalli on 4/9/2015.
 */
public class ProjectStorageDirectories {
    final public static String APP_DIRECTORY = "SolarSurvey/";
    final public static String PROJECT_DIRECTORY = "Projects/";
    final public static String LOG_DIRECTORY = "Logs/";

    final public static String PROPS_FILENAME = "ProjProps.csv";
    final public static String ARRAY_FILENAME = "array.csv";
    final public static String SHADING_FILENAME = "shading.csv";
    final public static String WEATHER_FILENAME = "weather.csv";
    final public static String SAMBEAM_FILENAME = "SAMHourlyShadingInput.csv";
    final public static String SAMDIFFUSE_FILENAME = "SAMDiffuseCorrectionFactorInput.csv";

    final public static String DATE_FORMAT = "MMddyyyy_kkmmss";

    private String mFileDir;

    public ProjectStorageDirectories(String directory){
        //get the storage directory
        File extFilesDir = Environment.getExternalStorageDirectory();
        //append the passed directory string to the storage director
        if (directory.contains(extFilesDir.toString())){ //Leave off storage dir if already present
            this.mFileDir = directory;
        } else {
            this.mFileDir = getAppDirectory() + PROJECT_DIRECTORY + directory;
        }
        //Append a slash if needed
        if (!this.mFileDir.substring(this.mFileDir.length()-1).equals("/")){
            this.mFileDir += "/";
        }

    }

    public String getProjectStorageDirectory(){
        return mFileDir;
    }

    public String getProjectImageDirectory(){
        return mFileDir + "Pictures/";
    }

    public static String getAppDirectory(){
        //get the storage directory
        File extFilesDir = Environment.getExternalStorageDirectory();
        return extFilesDir + "/" + APP_DIRECTORY;
    }

    public static String getProjectDirectory(){
        return getAppDirectory()+PROJECT_DIRECTORY;
    }

    public static String getTimeStamp(){
        DateFormat DF = new SimpleDateFormat(DATE_FORMAT);
        String reportDate = DF.format(Calendar.getInstance().getTime());
        return reportDate;
    }


}
