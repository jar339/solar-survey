package edu.psu.hn.solarsurvey.views;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import java.util.List;

import edu.psu.hn.solarsurvey.ControllerSingleton;
import edu.psu.hn.solarsurvey.R;
import edu.psu.hn.solarsurvey.io.filesave.ProjectFileData;
import edu.psu.hn.solarsurvey.model.Events.EventListener;

public class ProjectChooserActivity extends AppCompatActivity {

    String LOG_TAG = "ProjectChooserActivity";
    public static final String PROJECTNAME = "ProjectName";

    FloatingActionButton mFloatButton;
    ControllerSingleton controller;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_chooser);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        requestStoragePermission();

        controller = ControllerSingleton.getInstance();
        controller.addProjectFileListener(fileListListener);

        recyclerView = findViewById(R.id.projectCardRecycler);
        setupRecyclerView(controller.getExistingProjects());

        mFloatButton = findViewById(R.id.fab);
        mFloatButton.setOnClickListener(newProjectClick);

    }

    EventListener<List<ProjectFileData>> fileListListener = new EventListener<List<ProjectFileData>>() {
        @Override
        public void onEvent(List<ProjectFileData> projectFileData) {
            setupRecyclerView(projectFileData);
        }
    };


    private void setupRecyclerView(List<ProjectFileData> data){
        ProjectRecycleAdapter adapter = new ProjectRecycleAdapter(this,data);
        recyclerView.setAdapter(adapter);

        LinearLayoutManager layout = new LinearLayoutManager(this);
        layout.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layout);


        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    View.OnClickListener newProjectClick = new View.OnClickListener(){

        @Override
        public void onClick(View v) {
            controller.promptForFileName(ProjectChooserActivity.this);
        }
    };

    public static final int MY_PERMISSIONS_REQUEST_STORAGE = 100;

    /**
     * Perform a permission request for ACCESS_COARSE_LOCATION if not already active.
     * @return
     */
    public boolean requestStoragePermission() {
        Log.d(LOG_TAG,"Check storage permissions");
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(this)
                        .setTitle(R.string.request_storage_permission)
                        .setMessage(R.string.error_storage_permission)
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(ProjectChooserActivity.this,
                                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                        MY_PERMISSIONS_REQUEST_STORAGE);
                            }
                        })
                        .create()
                        .show();


            } else {
                Log.d(LOG_TAG,"Request write permission.");
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_STORAGE);
            }
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.d(LOG_TAG,"Permission for write granted.");
                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                } else {
                    Log.d(LOG_TAG,"Permission for write denied.");
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
            }
        }
    }

}
