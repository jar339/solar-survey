/*
 * This file is part of Solar Survey.
 *  Copyright Joe Ranalli 2014
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package edu.psu.hn.solarcalculations;

/**
 * Trigonometric functions that operate on arguments in degrees

 *
 */
public final class DegreeMath {
	
	//Wrappers for trigonometric functions in degrees
	/**
	 * Sin of angle in degrees
	 * @param angle in degrees
	 * @return
	 */
	public static double sind(double angdeg){
		return Math.sin(Math.toRadians(angdeg));
	}
	
	/**
	 * Cos of angle in degrees
	 * @param angle in degrees
	 * @return
	 */
	public static double cosd(double angdeg){
		return Math.cos(Math.toRadians(angdeg));
	}
	/**
	 * Tan of angle in degrees
	 * @param angle in degrees
	 * @return
	 */
	public static double tand(double angdeg){
		return Math.tan(Math.toRadians(angdeg));
	}
	/**
	 * Arcsin of angle in degrees
	 * @param angle in degrees
	 * @return
	 */
	public static double asind(double d){
		return Math.toDegrees(Math.asin(d));
	}
	/**
	 * Arccos of angle in degrees
	 * @param angle in degrees
	 * @return
	 */
	public static double acosd(double d){
		return Math.toDegrees(Math.acos(d));
	}
	/**
	 * Arctan of angle in degrees
	 * @param angle in degrees
	 * @return
	 */
	public static double atand(double d){
		return Math.toDegrees(Math.atan(d));
	}
	/**
	 * Arctan2 of angle in degrees
	 * @param y - the Y component
	 * @param x - the X component
	 * @return
	 */
	public static double atan2d(double y, double x){
		return Math.toDegrees(Math.atan2(y,x));
	}

}
