/*
 * This file is part of Solar Survey.
 *  Copyright Joe Ranalli 2014
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package edu.psu.hn.solarcalculations.irradiance;

import edu.psu.hn.solarcalculations.DegreeMath;

// Optimization

/**
 * Abstract implementation of Solar Resource models that can calculate POA irradiance from meterological data

 *
 */
public abstract class ResourceCalculator {

    protected abstract DiffuseIrradiance calcSkyDiffuse(double beamHorzIrrad, double diffHorzIrrad, double nDay, double incidenceAng, double sunAltitude, double collectorTilt);

    protected IIrradiance calcGroundReflected(double GHI, double collectorTilt, double reflectance){
        double GRI = (1- DegreeMath.cosd(collectorTilt))/2.0 * reflectance * GHI;
        return new Irradiance(Math.max(GRI,0));
    }

    protected IIrradiance calcPOABeamNormal(double beamHorzIrrad, double incidenceAng, double sunAltitude) {
        if ((incidenceAng>90) || (sunAltitude<0)){
            return new Irradiance(0);
        } else{
            double NPOA = beamHorzIrrad * DegreeMath.cosd(incidenceAng)/ DegreeMath.sind(sunAltitude);
            return new Irradiance(NPOA);
        }
    }

	// This is a global behavior not one that needs to be treated on a model by model basis 
	public double directNormalToBeamHorz(double directNorml, double sunAltitude){
		if (sunAltitude<0){
			return 0;
		} else{
			return directNorml* DegreeMath.sind(sunAltitude);
		}	
	}
    // This is a global behavior not one that needs to be treated on a model by model basis
    public double beamHorzToDirectNormal(double beamHorz, double sunAltitude){
        if (sunAltitude<=0){
            return 0;
        } else{
            return beamHorz/ DegreeMath.sind(sunAltitude);
        }
    }

}
