package edu.psu.hn.solarcalculations.irradiance;

public class Irradiance implements IIrradiance{
    private final double irradianceValue;

    Irradiance(double value){
        this.irradianceValue = value;
    }

    @Override
    public double getIrradiance() {
        return irradianceValue;
    }
}
