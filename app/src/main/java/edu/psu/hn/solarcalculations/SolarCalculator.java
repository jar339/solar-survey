/*
 * This file is part of Solar Survey.
 *  Copyright Joe Ranalli 2014
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package edu.psu.hn.solarcalculations;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import static edu.psu.hn.solarcalculations.DegreeMath.acosd;
import static edu.psu.hn.solarcalculations.DegreeMath.asind;
import static edu.psu.hn.solarcalculations.DegreeMath.cosd;
import static edu.psu.hn.solarcalculations.DegreeMath.sind;


public final class SolarCalculator {
	// Methodology based on PVEducation.org and Duffie & Beckman.  
	// Based on python code by J. Brownson. 
	
	
	
	/**
	 * 
	 * @param localTime
	 * @param day
	 * @param timeZone
	 * @param latitude
	 * @param longitude
	 * @return SunCoord referenced to zero degrees at North
	 */
	public static SunCoord calcSunPositionLST(double localTime, int day, int timeZone, double latitude, double longitude){
		
		double solarTime = calcSolarTime(localTime,day,timeZone,longitude);
		
		return calcSunPosition(solarTime, day, latitude);
		
	}

	/**
	 * 
	 * @param solarTime
	 * @param day
	 * @param latitude
	 * @return SunCoord referenced to North
	 */
	public static SunCoord calcSunPosition(double solarTime, int day, double latitude){		
		double altitude, azimuth;
		double declination = calcDeclination(day);
		double hourAngle = calcHourAngle(solarTime);
		
		altitude = calcSunAltitude(declination,latitude,hourAngle);
		//Use the North Reference
		azimuth = calcSunAzimuthMeteo(declination,latitude,hourAngle,90-altitude);
		
		SunCoord position = new SunCoord(azimuth, altitude);
		return position;
		
	}
	
	// Sun-Observer Geometry
	/**
	 * Solar Altitude
	 * @param declination in degrees
	 * @param latitude in degrees
	 * @param hourAngle in degrees
	 * @return the solar altitude in degrees
	 */
	public static double calcSunAltitude(double declination, double latitude, double hourAngle){
		double altitude;
		altitude = DegreeMath.asind(
							DegreeMath.sind(declination)* DegreeMath.sind(latitude) +
							DegreeMath.cosd(declination)* DegreeMath.cosd(latitude)* DegreeMath.cosd(hourAngle)
							);
		return altitude;
	}
	/**
	 * Solar Zenith Angle
	 * @param declination in degrees
	 * @param latitude in degrees
	 * @param hourAngle in degrees
	 * @return the solar zenith angle in degrees
	 */
	public static double calcSunZenith(double declination, double latitude, double hourAngle){
		return 90 - calcSunAltitude(declination,latitude,hourAngle);
	}
	/**
	 * The solar azimuth, referenced to 0° at due south
	 * @param declination in degrees
	 * @param latitude in degrees
	 * @param hourAngle in degrees 
	 * @param zenith in degrees
	 * @return the azimuth angle in degrees
	 */
	public static double calcSunAzimuth(double declination, double latitude, double hourAngle,double zenith){
		
		// //PVEducation Based Method 
		//double azimuth;
		//azimuth = -Math.signum(hourAngle)*Math.abs(	acosd(
		//									(sind(declination)*cosd(latitude) -
		//									cosd(declination)*sind(latitude)*cosd(hourAngle)) / cosd(altitude)
		//												));
		//azimuth = azimuth%360;
		//
		//if (azimuth<0){
		//	azimuth += 360;
		//}
		//return azimuth;
		
		//Duffie & Beckman
		double azimuth;
		
		int sign = (hourAngle>=0) ? 1 : -1; //Math.signum can return 0, which we don't want
		azimuth = sign * Math.abs(DegreeMath.acosd(
											( DegreeMath.cosd(zenith) * DegreeMath.sind(latitude) - DegreeMath.sind(declination) ) /
											( DegreeMath.sind(zenith) * DegreeMath.cosd(latitude) )
										));
		if (Double.isNaN(azimuth)){
			//This happens when the hour angle is close to 0 or +/-180.
			//For those cases the azimuth will be basically equal to the hour angle.
			azimuth = hourAngle;
		}
		return azimuth;
	}
	/**
	 * The solar azimuth, referenced to 0° pointing at the equator.  Calculates the
	 * zenith angle internally.  Use version with provided zenith angle to speed up.
	 * @param declination in degrees
	 * @param latitude in degrees
	 * @param hourAngle in degrees
	 * @return the azimuth angle in degrees
	 */
	public static double calcSunAzimuth(double declination, double latitude, double hourAngle){
		double zenith = calcSunZenith(declination,latitude,hourAngle);
		return calcSunAzimuth(declination,latitude,hourAngle,zenith);
	}
	/**
	 * The solar azimuth, referenced to 0° at due north
	 * @param declination in degrees
	 * @param latitude in degrees
	 * @param hourAngle in degrees 
	 * @param zenith in degrees
	 * @return the azimuth angle in degrees
	 */
	public static double calcSunAzimuthMeteo(double declination, double latitude, double hourAngle, double zenith){
		return calcSunAzimuth(declination,latitude,hourAngle,zenith) + 180;
	}
	/**
	 * The solar azimuth, referenced to 0° at due north.  Calculates the
	 * zenith angle internally.  Use version with provided zenith angle to speed up.
	 * @param declination in degrees
	 * @param latitude in degrees
	 * @param hourAngle in degrees 
	 * @return the azimuth angle in degrees
	 */
	public static double calcSunAzimuthMeteo(double declination, double latitude, double hourAngle){
		double zenith = calcSunZenith(declination,latitude,hourAngle);
		return calcSunAzimuthMeteo(declination,latitude,hourAngle,zenith);
	}
	/**
	 * The incidence angle between the sun and a collector
	 * @param declination in degrees
	 * @param latitude in degrees
	 * @param hourAngle in degrees
	 * @param collectorAzi in degrees
	 * @param collectorTilt in degrees
	 * @return the incidence angle in degrees
	 */
	public static double calcIncidenceAngle(double declination, double latitude, double hourAngle, double collectorAzi, double collectorTilt){
		double theta = DegreeMath.acosd( 	DegreeMath.sind(latitude) * DegreeMath.sind(declination) * DegreeMath.cosd(collectorTilt) -
                				DegreeMath.cosd(latitude) * DegreeMath.sind(declination) * DegreeMath.sind(collectorTilt) * DegreeMath.cosd(collectorAzi) +
                				DegreeMath.cosd(latitude) * DegreeMath.cosd(declination) * DegreeMath.cosd(collectorTilt) * DegreeMath.cosd(hourAngle) +
                				DegreeMath.sind(latitude) * DegreeMath.cosd(declination) * DegreeMath.sind(collectorTilt) * DegreeMath.cosd(collectorAzi) * DegreeMath.cosd(hourAngle) +
                				DegreeMath.cosd(declination) * DegreeMath.sind(collectorTilt) * DegreeMath.sind(collectorAzi) * DegreeMath.sind(hourAngle)
                			);
        return theta;
	}
	/**
	 * The incidence angle between the sun and a collector.  This version takes advantage of the solar position
	 * being known in advance.
	 * @param sunAzi in degrees
	 * @param sunAlt in degrees
	 * @param collectorAzi in degrees
	 * @param collectorTilt in degrees
	 * @return the incidence angle in degrees
	 */
	public static double calcIncidenceAngle(double sunAzi, double sunAlt, double collectorAzi, double collectorTilt){
		double theta = DegreeMath.acosd( 	DegreeMath.cosd(sunAlt) * DegreeMath.sind(collectorTilt) * DegreeMath.cosd(collectorAzi-sunAzi) +
								DegreeMath.sind(sunAlt) * DegreeMath.cosd(collectorTilt)
							);
		return theta;
	}
	
	//Astronomical
	// Calendar and Global Astronomical Manipulation
	/** 
	 * The day of the year from January 1
	 * @param calendar object set to correct date.  Time irrelevant.
	 * @return day of year
	 */
	public static int calcDayOfYear(Calendar calendar){
		return calendar.get(Calendar.DAY_OF_YEAR);
	}
	/**
	 * The day of the year from January 1
	 * @param month The month of year
	 * @param day The day of month (1 - January)
	 * @param year The year
	 * @return day of year
	 */
	public static int calcDayOfYear(int month, int day, int year){
		GregorianCalendar gc = new GregorianCalendar();
        gc.set(year, (month-1),day);
		return gc.get(Calendar.DAY_OF_YEAR);
	}
	/**
	 * The declination
	 * @param calendar set to correct date.  Time doesn't matter
	 * @return declination in degrees
	 */
	public static double calcDeclination(Calendar calendar){
		return calcDeclination(calcDayOfYear(calendar));
	}
	/**
	 * The declination
	 * @param nDay day of year as an integer
	 * @return the declination in degrees
	 */
	public static double calcDeclination(int nDay){
		//  //PVEducation.org Method
		//double declination = 23.45 * sind(360.0/365 * ( nDay - 81 )); //declination in degrees
        //return declination;
		
		//Duffie & Beckman method
		double B = ( nDay - 1 ) * 360.0 / 365.0;
        double delta = (0.006918 - 
                    0.399912 * DegreeMath.cosd( B ) +
                    0.070257 * DegreeMath.sind( B ) -
                    0.006758 * DegreeMath.cosd( 2 * B ) +
                    0.000907 * DegreeMath.sind( 2 * B ) -
                    0.002679 * DegreeMath.cosd( 3 * B ) +
                    0.001480 * DegreeMath.sind( 3 * B ));
        delta = Math.toDegrees(delta);
		
        return delta;
	}
	/** 
	 * The hour angle
	 * @param solarTime the solar time in hours, referenced to 12 at solar noon
	 * @return the hour angle in degrees referenced to 0° at solar noon
	 */
	public static double calcHourAngle(double solarTime){
		return 15 * (solarTime - 12);
	}

	//Time Correction & Conversion
	// Time conversions (Hours)
	/**
	 * Convert from solar time to local time.  Does not account for daylight savings, unless
	 * implicit in calendar time zone.
	 * @param solarTime the solar time in hours
	 * @param calendar a calendar set to the correct date, and the correct time zone.  Calendar's time irrelevant
	 * @param localLongitude the local longitude in degrees (+ for east, - for West
	 * @return the local standard time in hours.
	 */
	public static double calcLocalTime(double solarTime, Calendar calendar,double localLongitude){
		double EoT = calcEquationOfTime(calendar);
		double LongCorr = calcLongitudeCorr(calendar,localLongitude);
		return solarTime - EoT/60 - LongCorr/60;
	}
	/**
	 * Convert from solar time to local time. Does not account for daylight savings.
	 * @param solarTime the solar time in hours
	 * @param nDay the day of the year from Jan 1
	 * @param timeZone the time zone in hours from UTC (e.g. -5 for Philadelphia, PA)
	 * @param localLongitude the local longitude in degrees (+ for east, - for West)
	 * @return the local standard time in hours.
	 */
	public static double calcLocalTime(double solarTime, int nDay,int timeZone, double localLongitude){
		double EoT = calcEquationOfTime(nDay);
		double stdLongitude = 15 * timeZone;
		double LongCorr = calcLongitudeCorr(stdLongitude,localLongitude);
		return solarTime - EoT/60 - LongCorr/60;
	}
	/**
	 * Convert from local time to solar time.  Does not account for daylight savings, unless
	 * implicit in calendar time zone.
	 * @param localTime the local standard time in hours
	 * @param calendar a calendar set to the correct date, and the correct time zone.  Calendar's time irrelevant
	 * @param localLongitude the local longitude in degrees (+ for east, - for West
	 * @return the solar time in hours.
	 */
	public static double calcSolarTime(double localTime,Calendar calendar,double localLongitude){
		double EoT = calcEquationOfTime(calendar);
		double LongCorr = calcLongitudeCorr(calendar,localLongitude);
		return localTime + EoT/60 + LongCorr/60;
	}
	/**
	 * Convert from solar time to local time. Does not account for daylight savings.
	 * @param localTime the local standard time in hours
	 * @param nDay the day of the year from Jan 1
	 * @param timeZone the time zone in hours from UTC (e.g. -5 for Philadelphia, PA)
	 * @param localLongitude the local longitude in degrees
	 * @return the solar time in hours.
	 */
	public static double calcSolarTime(double localTime,int nDay,int timeZone, double localLongitude){
		double EoT = calcEquationOfTime(nDay);
		double stdLongitude = 15 * timeZone;
		double LongCorr = calcLongitudeCorr(stdLongitude,localLongitude);
		return localTime + EoT/60 + LongCorr/60;
	}
	
	//Time Corrections
	/**
	 * Equation of time correction
	 * @param calendar set to the correct date.  Time is ignored.
	 * @return The equation of time in minutes
	 */
	public static double calcEquationOfTime(Calendar calendar){
		// Equation of time in minutes
		return calcEquationOfTime(calcDayOfYear(calendar));
	}
	/**
	 * Equation of time correction
	 * @param nDay the day of the year since Jan 1.
	 * @return the equation of time in minutes
	 */
	public static double calcEquationOfTime(int nDay){
		// Equation of time in minutes
		
		//  //PVEducation Method
		//double B = 360.0/365.0 * (nDay - 81); //in degrees
		//double EoT = 9.87*sind(2*B)  - 7.53 * cosd(B) - 1.5 * sind(B);
		//return EoT;
		
		//Duffie & Beckman Method
		double B = (nDay-1) * (360.0 / 365.0);
		double EoT = 229.2 * (0.000075 + 0.001868* DegreeMath.cosd(B) -
		         0.032077 * DegreeMath.sind(B) - 0.014615 * DegreeMath.cosd( 2 * B ) -
		         0.04089 * DegreeMath.sind( 2 * B));
		
		return EoT;
	}
	/**
	 * Correction between local longitude and time zone standard longitude.  Uses +/- 0 to 180° for East/West longitude.
	 * @param standardLongitude the standard longitude for a time zone.  Shifts by 15 degrees per hour. (e.g. -75° for Eastern Time in US)
	 * @param localLongitude the actual local longitude
	 * @return the correction for longitude in minutes
	 */
	public static double calcLongitudeCorr(double standardLongitude, double localLongitude){
		double TC = 4 * (localLongitude - standardLongitude);
		return TC;
	}
	/**
	 * Correction between local longitude and time zone standard longitude
	 * @param calendar a calendar object with the time zone set.  date and time ignored.  Be careful of daylight savings
	 * @param localLongitude the actual local longitude
	 * @return the correction for longitude in minutes
	 */
	public static double calcLongitudeCorr(Calendar calendar, double localLongitude)
	{
		TimeZone timezone = calendar.getTimeZone();
		double tzOffset = timezone.getOffset(calendar.getTime().getTime()) / 1000 / 60 / 60;  //Time zone offset in hours
		
		double LSTM = 15 * tzOffset;
		
		double TC = calcLongitudeCorr(LSTM,localLongitude);
		return TC;
	}
}
