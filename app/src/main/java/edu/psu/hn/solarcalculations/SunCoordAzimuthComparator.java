/*
 * This file is part of Solar Survey.
 *  Copyright Joe Ranalli 2014
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package edu.psu.hn.solarcalculations;

import java.io.Serializable;
import java.util.Comparator;

/**
 * Comparator to test for equivalence between two SunCoords. Equivalence based on azimuth only. Unused

 *
 */
public class SunCoordAzimuthComparator implements Comparator<SunCoord>, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 818076562010157071L;

	@Override
	public int compare(SunCoord lhs, SunCoord rhs) {
		if (lhs!= null && rhs != null){
			
			if (lhs.azimuth<rhs.azimuth){
				return -1;
			} else if (lhs.azimuth == rhs.azimuth){
				return 0;
			} else {
				return 1;
			}
		} else {
			throw new NullPointerException("Cannot Compare null SunCoord");
		}
	}

}
