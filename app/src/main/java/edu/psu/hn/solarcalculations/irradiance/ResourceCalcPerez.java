/*
 * This file is part of Solar Survey.
 *  Copyright Joe Ranalli 2014
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package edu.psu.hn.solarcalculations.irradiance;

import edu.psu.hn.solarcalculations.DegreeMath;

/**
 * Resource calculator based on the Perez anisotropic sky model
 */
public class ResourceCalcPerez extends ResourceCalculator {
//    double[][] fs = new double[][]{ { -0.008,  0.588, -0.062, -0.06 ,  0.072, -0.022},
//                                    {   0.13,  0.683, -0.151, -0.019,  0.066, -0.029},
//                                    {   0.33,  0.487, -0.221,  0.055, -0.064, -0.026},
//                                    {  0.568,  0.187, -0.295,  0.109, -0.152, -0.014},
//                                    {  0.873, -0.392, -0.362,  0.226, -0.462,  0.001},
//                                    {  1.132, -1.237, -0.412,  0.288, -0.823,  0.056},
//                                    {   1.06,   -1.6, -0.359,  0.264, -1.127,  0.131},
//                                    {  0.678, -0.327,  -0.25,  0.156, -1.377,  0.251}};

    double[][] fs = new double[][]{ { -0.0083117,  0.5877285, -0.0620636, -0.0596012,  0.0721249, -0.0220216},
		  						  {  0.1299457,  0.6825954, -0.1513752, -0.0189325,  0.0659650, -0.0288748},
		  						  {  0.3296958,  0.4868735, -0.2210958,  0.0554140, -0.0639588, -0.0260542},
		  						  {  0.5682053,  0.1874525, -0.2951290,  0.1088631, -0.1519229, -0.0139754},
		  						  {  0.8730280, -0.3920403, -0.3616149,  0.2255647, -0.4620442,  0.0012448},
		  						  {  1.1326077, -1.2367284, -0.4118494,  0.2877813, -0.8230357,  0.0558651},
		  						  {  1.0601591, -1.5999137, -0.3589221,  0.2642124, -1.1272340,  0.1310694},
		  						  {  0.6777470, -0.3272588, -0.2504286,  0.1561313, -1.3765031,  0.2506212}};
	
    double [][] epsbins = new double[][] {  {0.000, 1.065} ,
                                            {1.065, 1.230},
                                            {1.230, 1.500},
                                            {1.500, 1.950},
                                            {1.950, 2.800},
                                            {2.800, 4.500},
                                            {4.500, 6.200},
                                            {6.200, 1e10},};

    @Override
    protected DiffuseIrradiance calcSkyDiffuse(double beamHorzIrrad, double diffHorzIrrad, double nDay, double incidenceAng, double sunAltitude, double collectorTilt) {
        // Ref http://pvpmc.org/modeling-steps/incident-irradiance/plane-of-array-poa-irradiance/calculating-poa-irradiance/poa-sky-diffuse/perez-sky-diffuse-model/
        // Updated Ref: https://sam.nrel.gov/reference
        double DPOA;
        double F1;
        double F2;

        double f11;
        double f12;
        double f13;
        double f21;
        double f22;
        double f23;

        double kappa = 5.534e-6; //5.535e-6 old
        double zen = 90 - sunAltitude;

        double a = Math.max(0, DegreeMath.cosd(incidenceAng));
        double b = Math.max(DegreeMath.cosd(85), DegreeMath.cosd(zen));


        double AM0 = 1/(b + 0.15*Math.pow(93.9-zen,-1.253)); 
        //double delta = diffHorzIrrad * 1/cosd(zen) * 1/(eps*1361);
        double delta = diffHorzIrrad * AM0/(1367.0);

        double directNormal = beamHorzToDirectNormal(beamHorzIrrad,sunAltitude);
        double epsPerez = ((directNormal+diffHorzIrrad)/diffHorzIrrad + kappa * Math.pow(zen,3))/(1+kappa*Math.pow(zen,3));
        int bin = getBin(epsPerez);

        double[] row = fs[bin];
        f11 = row[0];
        f12 = row[1];
        f13 = row[2];
        f21 = row[3];
        f22 = row[4];
        f23 = row[5];

        F1 = Math.max(0,f11 + f12*delta + f13*Math.PI*zen/180);
        F2 = f21 + f22*delta + f23*Math.PI*zen/180;

        double Di;
        double Dc;
        double Dh;
        
        if (zen<87.5)
        {
        	Di = diffHorzIrrad * ((1-F1)*(1+ DegreeMath.cosd(collectorTilt))/2);
        	Dc = diffHorzIrrad * F1*(a/b);
        	Dh = diffHorzIrrad * F2* DegreeMath.sind(collectorTilt);
        } else {
        	Di = diffHorzIrrad * (1+ DegreeMath.cosd(collectorTilt))/2;
        	Dc = 0;
        	Dh = 0;
        }

        return new DiffuseIrradiance(Dc,Di,Dh);

    }

    private int getBin(double eps){
        int L = epsbins.length;
        int bin = 0;
        for (double[] row:epsbins){
            if (eps<=row[1] && eps>row[0]){
                return bin;
            }
            bin++;
        }
        return L-1;
    }

}
