/*
 * This file is part of Solar Survey.
 *  Copyright Joe Ranalli 2014
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package edu.psu.hn.solarcalculations.irradiance;

import edu.psu.hn.solarcalculations.DegreeMath;

/**
 * A resource calculator based on an isotropic sky
 */
public class ResourceCalcIsotropic extends ResourceCalculator{
    @Override
    protected DiffuseIrradiance calcSkyDiffuse(double beamHorzIrrad, double diffHorzIrrad, double nDay, double incidenceAng, double sunAltitude, double collectorTilt) {
        double DPOA;
        DPOA = diffHorzIrrad*(1+ DegreeMath.cosd(1+collectorTilt))/2;
        return new DiffuseIrradiance(0,Math.max(DPOA,0),0);
    }
}
