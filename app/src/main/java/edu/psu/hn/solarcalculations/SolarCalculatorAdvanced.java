/*
 * This file is part of Solar Survey.
 *  Copyright Joe Ranalli 2014
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package edu.psu.hn.solarcalculations;

import static edu.psu.hn.solarcalculations.DegreeMath.acosd;
import static edu.psu.hn.solarcalculations.DegreeMath.asind;
import static edu.psu.hn.solarcalculations.DegreeMath.atand;
import static edu.psu.hn.solarcalculations.DegreeMath.cosd;
import static edu.psu.hn.solarcalculations.DegreeMath.sind;
import static edu.psu.hn.solarcalculations.DegreeMath.tand;

public final class SolarCalculatorAdvanced {

	/**
	 * The Julian fractional day of the year from January 1, 2000
	 * @param localTime The time in Local Standard Time in hours
     * @param nDay the day of the year since Jan 1 (should include leap year days)
	 * @param timeZone The time zone relative to UTC
	 * @param year The year
	 * @return julian day
     * @see <a href="http://sam.nrel.gov/reference">SAM Documentation</a> - PV Reference Manual
	 */
	public static double calcJulianDay(double localTime, int nDay, double timeZone, int year){
		double tutc = calcUTCTime(localTime,timeZone);
		return calcJulianDay(tutc, nDay, year);
	}

    /**
     * The Julian fractional day of the year from January 1, 2000
     * @param tutc the conventional UTC time in hours
     * @param nDay the day of the year since Jan 1  (should include leap year days)
     * @param year the year
     * @return julian day
     * @see <a href="http://sam.nrel.gov/reference">SAM Documentation</a> - PV Reference Manual
     */
	public static double calcJulianDay(double tutc, int nDay, int year){
		double jday = nDay;
		if (tutc<0)	{
			tutc = tutc+24;
			jday-=1;
		} else if (tutc>24) {
			tutc = tutc-24;
			jday += 1;
		}
		
		double julian = 32916.5 + 365 * (year-1949) + Math.floor((year-1949)/4.0) + jday + tutc/24.0 - 51545 ;
		return julian;
	}

    /**
     * Calculate the UTC time
     * @param localTime the Local Standard Time in hours
     * @param timeZone the timeZone from UTC in hours
     * @return the UTC time in hours
     * @see <a href="http://sam.nrel.gov/reference">SAM Documentation</a> - PV Reference Manual
     */
	public static double calcUTCTime(double localTime, double timeZone){
		double tutc = localTime + 30.0/60.0 - timeZone;
		return tutc;
	}

    /**
     * Calculate the mean anomaly used in sun position calculations
     * @param julian the Julian fractional day
     * @return the Mean Anomaly in degrees
     * @see <a href="http://sam.nrel.gov/reference">SAM Documentation</a> - PV Reference Manual
     */
	public static double calcMeanAnomaly(double julian){
		double mnanom = (357.528 + 0.9856003 * julian)%360.0;
		return mnanom;
	}

    /**
     * Calculate the mean longitude used in sun position calculations
     * @param julian the Julian fractional day
     * @return the Mean Longitude in degrees
     * @see <a href="http://sam.nrel.gov/reference">SAM Documentation</a> - PV Reference Manual
     */
	public static double calcMeanLongitude(double julian){
		double mnlong = (280.46 + 0.9856474 * julian)%360.0;
		return mnlong;
	}

    /**
     * Calculate the ecliptic longitude used in sun position calculations
     * @param julian the Julian fractional day
     * @return the Ecliptic Longitude in degrees
     * @see <a href="http://sam.nrel.gov/reference">SAM Documentation</a> - PV Reference Manual
     */
	public static double calcEclipticLong(double julian){
		double mnanom = calcMeanAnomaly(julian);
		double eclong = calcMeanLongitude(julian) + 1.915* DegreeMath.sind(mnanom)+0.02* DegreeMath.sind(2*mnanom);
		return eclong;
	}

    /**
     * Calculate the obliquity of the ecliptic used in sun position calculations
     * @param julian the Julian fractional day
     * @return the ecliptic obliquity in degrees
     * @see <a href="http://sam.nrel.gov/reference">SAM Documentation</a> - PV Reference Manual
     */
	public static double calcObliquityofEcliptic(double julian){
		double obleq = 23.439 - 0.0000004*julian;
		return obleq;
	}

    /**
     * Calculate the declination
     * @param julian the Julian fractional day (@see calcJulianDay())
     * @return the declination in degrees
     * @see <a href="http://sam.nrel.gov/reference">SAM Documentation</a> - PV Reference Manual
     */
	public static double calcDeclination(double julian){
		double obleq = calcObliquityofEcliptic(julian);
		double eclong = calcEclipticLong(julian);
		double declination = DegreeMath.asind(DegreeMath.sind(obleq)* DegreeMath.sind(eclong));
		return declination;
	}

    /**
     * Calculate the right ascension used in sun position calculations
     * @param julian the Julian fractional day
     * @return the right ascension in degrees
     * @see <a href="http://sam.nrel.gov/reference">SAM Documentation</a> - PV Reference Manual
     */
	public static double calcRightAscension(double julian){
		double ra;
		double obleq = calcObliquityofEcliptic(julian);
		double eclong = calcEclipticLong(julian);
		if (DegreeMath.cosd(eclong)<0) {
			ra = DegreeMath.atand(DegreeMath.cosd(obleq)* DegreeMath.sind(eclong)/ DegreeMath.cosd(eclong))+180;
		} else{
			ra = DegreeMath.atand(DegreeMath.cosd(obleq)* DegreeMath.sind(eclong)/ DegreeMath.cosd(eclong))+360;
		}
		return ra;
	}

    /**
     * Calculate the hour angle
     * @param julian the Julian fractional day (@see calcJulianDay())
     * @param tutc the conventional UTC time in hours
     * @param longitude the local longitude in degrees
     * @return the hour angle in degrees
     * @see <a href="http://sam.nrel.gov/reference">SAM Documentation</a> - PV Reference Manual
     */
	public static double calcHourAngle(double julian, double tutc, double longitude){
		double gmst = 6.697375 + 0.0657098242*julian + tutc;
		double lmst = gmst + longitude/15;
		double HA = (15*lmst - calcRightAscension(julian))%360.0; 
		if (HA<-180){
			HA += 360; 
		} else if (HA>180){
			HA-=360;
		}
		return HA;
	}

    /**
     * Calculate the solar altitude, applying a correction for atmospheric refraction
     * @param declination the declination in degrees
     * @param latitude the local latitude in degrees
     * @param hourAngle the hour angle in degrees
     * @return the corrected altitude in degrees
     * @see <a href="http://sam.nrel.gov/reference">SAM Documentation</a> - PV Reference Manual
     */
	public static double calcAltitudeCorrected(double declination, double latitude, double hourAngle){
		double a0 = calcAltitude(declination, latitude, hourAngle);
		
		double r = a0 + 3.51561 * (0.1594+0.0196*a0+0.00002*a0*a0)/(1.0+0.505*a0+0.0845*a0*a0);
		if (r <= 0.56){
			r = -0.56;
		}
		
		double altitude = Math.min(r, 90);
		return altitude;
	}

    /**
     * Calculate the solar altitude (no correction for atmospheric refraction applied)
     * @see "calcAltitudeCorrected()"
     * @param declination the declination in degrees
     * @param latitude the local latitude in degrees
     * @param hourAngle the hour angle in degrees
     * @return the solar altitude in degrees
     * @see <a href="http://sam.nrel.gov/reference">SAM Documentation</a> - PV Reference Manual
     */
	public static double calcAltitude(double declination, double latitude, double hourAngle){
		double ref_alt = DegreeMath.sind(declination)* DegreeMath.sind(latitude) + DegreeMath.cosd(declination)* DegreeMath.cosd(latitude)* DegreeMath.cosd(hourAngle);
		double a0 = DegreeMath.asind(ref_alt);
		if (ref_alt<-1){
			a0 = -90;
		} else if (ref_alt>1){
			a0 = 90;
		}
		
		
		return a0;
	}

    /**
     * Calculate the solar azimuth referenced to 0° as north, and varying from 0-360
     * @param declination the declination in degrees
     * @param latitude the local latitude in degrees
     * @param hourAngle the hour angle in degrees
     * @return the azimuth in degrees
     * @see <a href="http://sam.nrel.gov/reference">SAM Documentation</a> - PV Reference Manual
     */
	public static double calcAzimuth(double declination, double latitude, double hourAngle){
		double a0 = calcAltitude(declination, latitude, hourAngle);
		double azimuth;
		double a = (DegreeMath.sind(a0)* DegreeMath.sind(latitude)- DegreeMath.sind(declination))/(DegreeMath.cosd(a0)* DegreeMath.cosd(latitude));
		double b = DegreeMath.acosd(a);
		if (DegreeMath.cosd(a0)==0 || a<-1){
			b = 180;
		} else if (a>1){
			b = 0;
		}
		
		if (hourAngle<-180){
			azimuth = b;	
		}else if (hourAngle<0 || hourAngle>180){
			azimuth = 180-b;
		}else {
			azimuth = 180+b;
		}
		azimuth %= 360.0;
		return azimuth;
	}

    /**
     * Calculate a corrected "mid-hour" time for hours where sunrise or sunset occur
     * Returns zero if sunrise/sunset does not occur during this hour
     * @param localtime Local Standard Time in hours
     * @param latitude the local latitude in degrees
     * @param longitude the local longitude in degrees
     * @param declination the declination in degrees
     * @param timeZone the timezone from UTC in hours
     * @param julian the Julian fractional day (@see calcJulianDay())
     * @return the corrected mid-hour time for special hours. Zero if sunset/sunrise not in this hour
     */
	public static double calcSRSSTimeFix(double localtime, double latitude, double longitude, double declination, int timeZone, double julian){
		
		double ra = calcRightAscension(julian);
		double mnlong = calcMeanLongitude(julian);
		
		double a = -DegreeMath.tand(latitude)* DegreeMath.tand(declination);
		double HAR = DegreeMath.acosd(a);
		if (a>1){
			HAR = 0;
		} else if (a<-1) {
			HAR = 180;
		}
		
		a = 1/15.0*((mnlong - ra)%360.0);
		double EOT = a;
		if (a<-0.33){
			EOT += 24;
		} else if (a>0.33){
			EOT -= 24;
		}
		
		double tsr = 12-1/15.0*(HAR) - (longitude/15.0-timeZone) - EOT;
		double tss = 12+1/15.0*(HAR) - (longitude/15.0-timeZone) - EOT;
		int flag = 0;
		double newtime=0;
		if (localtime<tsr && localtime+1>tsr){
			flag = 2; //sunrise
			newtime = ((localtime + 1) + tsr)/2.0;
		} else if (localtime<tss && localtime+1>tss) {
			flag = 3; //sunset
			newtime = ((localtime+tss)/2.0);
		}
		return newtime;
	}
}