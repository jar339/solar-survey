/*
 * This file is part of Solar Survey.
 *  Copyright Joe Ranalli 2014
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package edu.psu.hn.solarcalculations;

import java.io.Serializable;

/**
 * Holder for latitude and longitude pairs

 *
 */
public class SurfCoord implements Serializable{
	public double latitude;
	public double longitude;
	
	public SurfCoord(double lat, double lon){
		latitude = lat;
		longitude = lon;
	}
	
	public String toString() {
		return "(lat:" +this.latitude+",lon:"+this.longitude+")";
	}
}
