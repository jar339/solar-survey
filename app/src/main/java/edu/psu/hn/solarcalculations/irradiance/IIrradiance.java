package edu.psu.hn.solarcalculations.irradiance;

public interface IIrradiance {
    /**
     *
     * @return the total irradiance
     */
    public double getIrradiance();


}
