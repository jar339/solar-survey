/*
 * This file is part of Solar Survey.
 *  Copyright Joe Ranalli 2014
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package edu.psu.hn.solarcalculations;

import java.io.Serializable;

/**
 * A holder object for sun's position in the sky.

 *
 */
public class SunCoord implements Serializable{
	public double altitude;
	public double azimuth;
	
	public SunCoord(double azi, double alt){
		set(azi,alt);
	}
	
	public String toString(){
		return String.format("(Azi: %f), (Alt: %f)",azimuth, altitude);
	}
	
	public String toCoord(){
		return String.format("(%f,%f)",azimuth,altitude);
	}

    public final boolean equals (SunCoord p){
        if (p.altitude == altitude && p.azimuth == azimuth){
            return true;
        }
        return false;
    }

    public final void set(double azi, double alt){
        azimuth = azi; altitude = alt;
        unwrap();
    }

    public final void offset(double dAzi, double dAlt){
        azimuth+=dAzi;
        altitude+=dAlt;
        unwrap();
    }

    private void unwrap(){
        while (azimuth>=360){
            azimuth -= 360;
        }
        if (altitude>90){
            altitude = 180 - altitude;
            azimuth = (azimuth + 180)%360.0;
        }
    }
}
