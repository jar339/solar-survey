package edu.psu.hn.solarcalculations.irradiance;

public class DiffuseIrradiance implements IIrradiance {
    private double circumsolar;
    private double isotropic;
    private double horizon;

    /**
     * Diffuse Irradiance
     * @param circumsolar the circumsolar component
     * @param isotropic the isotropic component
     * @param horizon the horizon brightening component
     */
    DiffuseIrradiance(double circumsolar, double isotropic, double horizon) {
        this.circumsolar = circumsolar;
        this.isotropic = isotropic;
        this.horizon = horizon;
    }

    @Override
    public double getIrradiance() {
        return circumsolar + isotropic + horizon;
    }
}
