/*
 * This file is part of Solar Survey.
 *  Copyright Joe Ranalli 2014
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package edu.psu.hn.solarcalculations.irradiance;

import edu.psu.hn.solarcalculations.DegreeMath;

/**
 * Muneer's method for calculating POA irradiance based on meterological data
 *
 */
public class ResourceCalcMuneer extends ResourceCalculator {

	@Override
	protected DiffuseIrradiance calcSkyDiffuse(double beamHorzIrrad, double diffHorzIrrad, double nDay, double incidenceAng, double sunAltitude, double collectorTilt){
		double eps = 1 + 0.033* DegreeMath.cosd(360.0*(nDay-2)/365.0); //PVEducation.org and elsewhere
		double Kb = beamHorzIrrad/(eps*1361* DegreeMath.sind(sunAltitude));
		double fB = Math.pow(DegreeMath.cosd(collectorTilt/2),2)
				+ (0.00263 - 0.7120 * Kb - 0.6883  *Math.pow(Kb,2) ) 
				* (DegreeMath.sind(collectorTilt) - Math.PI/180 * collectorTilt * DegreeMath.cosd(collectorTilt)
						- Math.PI * Math.pow( DegreeMath.sind(collectorTilt/2),2 )
						);
		
		double circSolar;
		double skyBkg;
		if (sunAltitude>5.7){
            circSolar = diffHorzIrrad * Kb * DegreeMath.cosd(incidenceAng) / DegreeMath.sind(sunAltitude);
            skyBkg = diffHorzIrrad * fB * (1-Kb);
		} else {
			circSolar = 0;
			skyBkg = diffHorzIrrad *
							Math.pow(DegreeMath.cosd(collectorTilt/2),2) *
							(1+Kb*Math.pow(DegreeMath.sind(collectorTilt/2),3)) *
							(1+Kb*Math.pow(DegreeMath.cosd(incidenceAng),2)*Math.pow(DegreeMath.sind(90-sunAltitude),3));
		}
		return new DiffuseIrradiance(Math.max(circSolar,0),Math.max(skyBkg,0),0);
	}
}
